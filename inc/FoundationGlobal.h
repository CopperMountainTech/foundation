#pragma once

#include <QtCore/qglobal.h>
#include <QtCore/qobjectdefs.h>
#include <QHash>
#include "qmath.h"

//#define STACKUNDO

#if defined(FOUNDATION_LIBRARY)
#  define FOUNDATIONSHARED_EXPORT Q_DECL_EXPORT
#else
#  define FOUNDATIONSHARED_EXPORT Q_DECL_IMPORT
#endif

namespace Planar {

#ifndef M_PI
#define M_PI 3.14159265358979323846   // compatibility
#endif

const double PI = M_PI;
const double TwoPI = 2 * M_PI;

FOUNDATIONSHARED_EXPORT Q_NAMESPACE

enum class CommandType
{
    defaultType = 0,
    WaitChannelStop = defaultType, // Дождаться остановки потока
    StopCurrentChannel,           // Остановить поток и выполнить команду
    StopAllChannels,              // Остановить все потоки и выполнить команду
    Immediate                    // Возможно выполнение немедленно, без остановки потока
};
Q_ENUM_NS(CommandType);

enum class CommandStatus
{
    defaultStatus = 0,
    InQueue = defaultStatus,    //команда находится в очереди на обработку
    Running,                    //выполняется в данный момент
    Completed,                  //выполнена, данные о прошлом состоянии сохранены
    Error                       //команда не существует
};
Q_ENUM_NS(CommandStatus);

enum class PushingType
{
    defaultType = 0,
    ToBack = defaultType,    //добавление команды по умолчанию в конец очереди
    ToFront                  //добавление в начало очереди (исп-ся при redo(IsRunning))
};
Q_ENUM_NS(PushingType);

enum class StackUndoPushResult
{
    NotPushed,
    Pushed
};
Q_ENUM_NS(StackUndoPushResult);

} //namespace Planar

#include <QString>
#include <QSettings>
#include <QObject>
#include <QDebug>
#include <QMetaObject>
#include <QMetaMethod>
#include <QPointer>
