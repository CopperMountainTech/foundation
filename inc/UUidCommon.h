#pragma once

#include <QtCore/qglobal.h>
#include <QtCore/qobjectdefs.h>
#include <QUuid>

#define PLANAR_UUID "{5986d27c-e7dc-49b8-ad8a-02fa8ba3bcf7}"

#define ANALYZER_ITEM_UUID(ns)                                  \
    public:                                                     \
    static QUuid staticUuid() {                                 \
        static QUuid uuid = QUuid::createUuidV5( QUuid(ns),     \
                    QString(staticMetaObject.className()));     \
        return uuid; }                                          \
    QUuid uuid() const override { return staticUuid(); }        \
    static QUuid staticPropertyUuid(QString propertyName) {     \
        static QHash<QString, QUuid> _propsUuidMap =            \
            generateStaticPropertiesUuidMap(                    \
                staticUuid(), staticMetaObject);                \
        return generateStaticPropertyUuid(                      \
            _propsUuidMap, propertyName); }                     \
    QUuid propertyUuid(QString propertyName) const override {   \
        return staticPropertyUuid(propertyName); } \
    protected: \
    void initUuid() override { return; }

#define ANALYZER_ITEM_DYNAMIC_UUID(ns, propName)                \
    public:                                                     \
    static QUuid staticUuid() {                                 \
        static QUuid uuid = QUuid::createUuidV5( QUuid(ns),     \
                    QString(staticMetaObject.className()));     \
        return uuid; }                                          \
                                                                \
    static QUuid staticUuid(QVariant value) {                   \
        if(value.canConvert<QObject*>()) {                      \
            /*потенциально наследник PropertyBase*/             \
            QObject* obj = value.value<QObject*>();             \
            const QMetaObject* meta = obj->metaObject();        \
            QMetaProperty prop =                                \
                meta->property(meta->indexOfProperty("value")); \
            value = prop.read(obj);                             \
        }                                                       \
        QUuid dependentUuid = QUuid::createUuidV5(staticUuid(), \
            value.toByteArray());                               \
        return dependentUuid;}                                  \
                                                                \
    QUuid uuid() const override {                               \
        const QMetaObject* meta = metaObject();                 \
        QMetaProperty prop =                                    \
            meta->property(meta->indexOfProperty(#propName));   \
        QVariant value = prop.read(this);                       \
        return staticUuid(value); }                             \
                                                                \
    QUuid originUuid() const { return staticUuid(); }           \
                                                                \
    static QUuid staticPropertyUuid(QString propertyName) {     \
        static QHash<QString, QUuid> _propsUuidMap =            \
            generateStaticPropertiesUuidMap(                    \
                staticUuid(), staticMetaObject);                \
        return generateStaticPropertyUuid(                      \
            _propsUuidMap, propertyName); }                     \
                                                                \
    QUuid propertyUuid(QString propertyName) const override {   \
        return staticPropertyUuid(propertyName); }              \
    void initUuid() override {                                  \
        const QMetaObject* meta = metaObject();                 \
        QMetaProperty prop = meta->property(                    \
            meta->indexOfProperty(#propName));                  \
        QVariant value = prop.read(this);                       \
        QObject* obj;                                           \
        if(value.canConvert<QObject*>()) {                      \
            /*потенциально наследник PropertyBase*/             \
            obj = value.value<QObject*>();                      \
            const QMetaObject* meta = obj->metaObject();        \
            prop = meta->property(                              \
                meta->indexOfProperty("value"));                \
            if (prop.hasNotifySignal()) {                       \
                QMetaMethod signal = prop.notifySignal();       \
                QMetaMethod updateSlot = metaObject()->method(  \
                    metaObject()->indexOfSlot(                  \
                        "onPropertyChangedUuid()"));            \
            connect(obj, signal, this, updateSlot); }}}

//----------------------------------------------------------------------------

