#ifndef ROOTBASEITEM_H
#define ROOTBASEITEM_H

#include <QQueue>

#include "AnalyzerItem.h"
#include "UUidCommon.h"
#include "AnalyzerCommand.h"
#include "StackUndo/StackUndoDispatcher.h"

namespace Planar {

class FOUNDATIONSHARED_EXPORT RootBaseItem : public AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
public:
    ~RootBaseItem() override;

    void pushCommand(const AnalyzerCommand& command,
                     PushingType pushType = PushingType::defaultType) override;
    void addCommandToQueue(const AnalyzerCommand& command,
                           PushingType pushType = PushingType::defaultType) override;

    StackUndoDispatcher* undoDispatcher() override
    {
        return &_undoDispatcher;
    }

    virtual QQueue<AnalyzerCommand>* commandQueue() override
    {
        return &_commandQueue;
    }

signals:
    void commandAppended();
    void rootPropertyAdded();

public slots:
    void performUndoNeeded();

private slots:
    void rootThreadReady();

protected:
    RootBaseItem();
    StackUndoDispatcher _undoDispatcher;

    //очередь команд
    QQueue<AnalyzerCommand> _commandQueue;
};

} //namespace Planar

#endif // ROOTBASEITEM_H

