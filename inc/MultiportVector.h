#pragma once

#include "FoundationGlobal.h"

#include <vector>
#include <algorithm>

namespace Planar {

template<class T>
struct MultiportVector : public std::vector<std::vector<T>>
{
    MultiportVector(size_t portCount) : std::vector<std::vector<T>>(portCount) {}
    MultiportVector(size_t portCount, size_t pointCount) : std::vector<std::vector<T>>(portCount,
                                                                                           std::vector<T>(pointCount)) {}

    size_t portCount() const
    {
        return this->size();
    }

    size_t pointCount() const
    {
        if (portCount() == 0) return 0;
        return this->front().size();     // sic, this for gcc
    }

    void resize(size_t newPointCount)
    {
        Q_ASSERT(newPointCount > 0);

        std::for_each(this->begin(), this->end(), [newPointCount](std::vector<T>& ivec)
        {
            ivec.resize(newPointCount);
        });
    }

    void reserve(size_t newPointCount)
    {
        Q_ASSERT(newPointCount > 0);

        std::for_each(this->begin(), this->end(), [newPointCount](std::vector<T>& ivec)
        {
            ivec.reserve(newPointCount);
        });
    }

    void clear()
    {
        std::for_each(this->begin(), this->end(), [](std::vector<T>& ivec)
        {
            ivec.clear();
        });
    }

    void pushData(T value)
    {
        std::for_each(this->begin(), this->end(), [&value](std::vector<T>& ivec)
        {
            ivec.push_back(value);
        });
    }

    void pushData(const std::vector<T>& data)
    {
        Q_ASSERT(portCount() == data.size());

        for (size_t i = 0; i < this->size(); ++i)
        {
            (*this)[i].push_back(data[i]);
        }
    }

    void readData(size_t column, std::vector<T>& data) const
    {
        Q_ASSERT(portCount() == data.size());
        Q_ASSERT(column < portCount());

        for (size_t i = 0; i < this->size(); ++i)
        {
            data[i] = (*this)[i][column];
        }
    }

    void setData(size_t column, const std::vector<T>& data)
    {
        Q_ASSERT(portCount() == data.size());
        Q_ASSERT(column < portCount());

        for (size_t i = 0; i < this->size(); ++i)
        {
            (*this)[i][column] = data[i];
        }
    }

    void setData(size_t column, T value)
    {
        Q_ASSERT(column < portCount());

        for (size_t i = 0; i < this->size(); ++i)
        {
            (*this)[i][column] = value;
        }
    }
};

template<class T>
struct MultiportVector2D : public std::vector<MultiportVector<T>>
{
    MultiportVector2D(size_t portCount, size_t pointCount)
        : std::vector<MultiportVector<T>>(portCount, MultiportVector<T>(portCount, pointCount))
    {
    }

    size_t portCount() const
    {
        return this->size();
    }

    size_t pointCount() const
    {
        if (portCount() == 0) return 0;
        return this->front().size();
    }

    void resize(size_t newPointCount)
    {
        Q_ASSERT(newPointCount > 0);

        std::for_each(this->begin(), this->end(), [newPointCount](std::vector<std::vector<T>>& jvec)
        {
            std::for_each(jvec.begin(), jvec.end(), [newPointCount](std::vector<T>& ivec)
            {
                ivec.resize(newPointCount);
            });
        });
    }

    void reserve(size_t newPointCount)
    {
        Q_ASSERT(newPointCount > 0);

        std::for_each(this->begin(), this->end(), [newPointCount](std::vector<std::vector<T>>& jvec)
        {
            std::for_each(jvec.begin(), jvec.end(), [newPointCount](std::vector<T>& ivec)
            {
                ivec.reserve(newPointCount);
            });
        });
    }

    T data(size_t x, size_t y, size_t index)
    {
        return (*this)[x][y][index];
    }
    T data(size_t pos, size_t index)
    {
        return data(pos, pos, index);
    }

    void setData(size_t x, size_t y, size_t index, T value)
    {
        (*this)[x][y][index] = value;
    }
    void setData(size_t pos, size_t index, T value)
    {
        setData(pos, pos, index, value);
    }
};

} // namespace
