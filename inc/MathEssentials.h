#pragma once

#include "FoundationGlobal.h"
#include <ctime>

namespace Planar {

const double DoubleEpsilon = 10E-12;

const double Log2FromE = 1.44269504088896340736; //log2(e)

inline long double log2(const long double x)
{
    return  log(x) * Log2FromE;
}

template<typename T>
T ensureRange(T value, T min, T max)
{
    if (value < min) return min;
    if (value > max) return max;
    return value;
}

inline bool isZero(double value)
{
    return qFuzzyIsNull(value);
}

inline bool isEqual(double v1, double v2)
{
    return (qFuzzyCompare(v1, v2));
}

//---------------------------------------------------------------------------

inline int intRound(double value)
{
    return value >= 0 ? int(value + 0.5) : int(value - 0.5);
}
inline double  roundBy(double r, double step)
{
    return step * floor(r / step + 0.5);
}
inline double  roundUp(double r, double step)
{
    return step * ceil(r / step);
}
inline double  roundDown(double r, double step)
{
    return step * floor(r / step);
}

/// \brief Переводит отношение значений энергетической величины(звуковое давление,
/// электрическое напряжение, сила электрического тока и т. п.) из безразменой единицы в децибелы
/// \param[in] ratio отношение в безразмерной еденице
/// \return отношение значений энергетической величины, выраженное в децибелах
inline double decibel(const double ratio)
{
    return 20.0 * log10(ratio);
}

/// \brief Переводит отношение значений силовой величины(мощность, энергия, плотность энергии и т. п.)
/// из безразменой единицы в децибелы
/// \param[in] ratio отношение в безразмерной еденице
/// \return отношение значений силовой величины, выраженное в децибелах
inline double decibelPower(const double ratio)
{
    return 10.0 * log10(ratio);
}

/// \brief Переводит отношение значений силовой величины(мощность, энергия, плотность энергии и т. п.)
/// из безразменой единицы в децибелы
/// \param[in] ratio отношение в безразмерной еденице
/// \return отношение значений силовой величины, выраженное в децибелах
inline float decibelPowerF(const float ratio)
{
    return 10.0f * log10f(ratio);
}

/// \brief Переводит отношение значений энергетической величины(звуковое давление,
/// электрическое напряжение, сила электрического тока и т. п.) из децибел в безразменую единицу
/// \param[in] db отношение в децибелах
/// \return отношение в берзразмерной еденице
inline double decibelToUnity(const double db)
{
    return pow(10., db / 20.);
}


/// \brief Переводит отношение значений силовой величины(мощность, энергия, плотность энергии и т. п.)
/// из децибел в безразменую единицу
/// \param[in] db отношение в децибелах
/// \return отношение в берзразмерной еденице
inline double decibelPowerToUnity(const double db)
{
    return pow(10., db / 10.);
}

///
/// \brief Округляет вещественнное число одинарной точности до ближайшего целого с учётом знака
/// \param[in] a вещественное число
/// \return округленное целое
///
inline float roundF(const float a)
{
    return a < 0.0f ? ceilf(a - 0.5f) : floorf(a + 0.5f);
}

///
/// \brief Отбрасывает дробную часть вещественного числа с учётом знака
/// \param[in] a вещественное число
/// \return число с отброшенной дробной частью
///
inline float truncF(const float a)
{
    return a < 0.0f ? ceilf(a) : floorf(a);
}

///
/// \brief Ограничивает число в диапазоне от 0 до a
/// \param[in] a - граница
/// \param[in] thres исходное число
/// \return
///
inline float saturateF(const float a, const float thres)
{
    float th = fabs(thres);
    return (std::min)(th, (std::max)(-th, a));
}


///
/// \brief Функция знака беззнакового числа
/// \param[in] x число
/// \param[in] is_signed
/// \return
///
template<class T> inline int signum(const T x, std::false_type is_signed)
{
    return T(0) < x;
}

///
/// \brief Функция знака знакового числа
/// \param[in] x число
/// \param[in] is_signed
/// \return
///
template<class T> inline int signum(const T x, std::true_type is_signed)
{
    return (T(0) < x) - (x < T(0));
}

///
/// \brief Функция знака числа
/// \param[in] x число
/// \return 1 если x>0, -1 если x<0, 0 если x=0
///
template<class T> inline int signum(const T x)
{
    return signum(x, std::is_signed<T>());
}

FOUNDATIONSHARED_EXPORT double setPrecision(const double Value, const int Precision);
FOUNDATIONSHARED_EXPORT void valueIncrement(double MinDelta, double& Value, int Rate);

FOUNDATIONSHARED_EXPORT double normalizeRadianPhase(double phase);
FOUNDATIONSHARED_EXPORT double normalizeDegreePhase(double phase);


///
/// \brief Генерирует случайное вещественное число в заданном диапазоне
/// \param[in] minVal нижнаяя граница диапазона
/// \param[in] maxVal верхняя граница диапазона
/// \return случайное число
///
FOUNDATIONSHARED_EXPORT double randomValue(const double minVal, const double maxVal);


}



