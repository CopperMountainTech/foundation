#ifndef PROPERTYBASE_H
#define PROPERTYBASE_H

#include "FoundationGlobal.h"
#include <QMetaType>
#include <QObject>

namespace Planar {

class FOUNDATIONSHARED_EXPORT PropertyBase : public QObject
{
    Q_OBJECT
protected:
    explicit PropertyBase(QObject* parent = nullptr);
};

}

#endif // PROPERTYBASE_H
