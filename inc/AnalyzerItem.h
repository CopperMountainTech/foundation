#pragma once

#include <QMutex>
#include <QQueue>
#include <QXmlStreamWriter>

#include "FoundationGlobal.h"
#include "UUidCommon.h"
#include "AnalyzerCommand.h"
#include "StackUndo/StackUndoDispatcher.h"
#include "Properties/CommandProperty.h"
#include "Properties/IntProperty.h"
#include "Properties/BoolProperty.h"

namespace Planar {

class AnalyzerItemCollectionBase;

class FOUNDATIONSHARED_EXPORT AnalyzerItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int collectionIndex READ collectionIndex NOTIFY collectionIndexChanged)
    Q_PROPERTY(AnalyzerItem* parent READ parentItem CONSTANT)
    Q_PROPERTY(AnalyzerItem* propertyExtension READ propertyExtension WRITE setPropertyExtension NOTIFY
               propertyExtensionChanged)
    Q_PROPERTY(QUuid uuid READ uuid NOTIFY uuidChanged)
    Q_PROPERTY(bool isActive READ isActive WRITE setActive NOTIFY activeStateChanged)

public:
    AnalyzerItem(AnalyzerItem* parentItem);

    virtual ~AnalyzerItem();

    // указатель на родительский item
    AnalyzerItem* parentItem() const;

    // указатель на дочерний item по индексу
    // не путать с индексом в коллекции,
    // может быть полезен только при сканировании
    AnalyzerItem* childItem(int index) const;

    // число дочерних элементов
    // не путать с числом элементов коллекций
    int childCount() const;

    // список всех дочерних элементов
    QList<AnalyzerItem*>& childItems()
    {
        return _childItems;
    }

    // указатель на коллекцию, содержащую item
    // nullptr, если item не принадлежит коллекции
    AnalyzerItemCollectionBase* collection() const
    {
        return _containingCollection;
    }

    // запрос на удаление из коллекции
    // не будет обработан, если item не принадлежит коллекции
    Q_INVOKABLE void requestRemoveFromCollection();

    // индекс в коллекции. -1, если не принадлежит коллекции
    int collectionIndex()
    {
        return _collectionIndex;
    }

    // признак активности в коллекции
    // краткое названия для читабельности. по сути isActiveInCollection
    bool isActive() const
    {
        return _isActive;
    }

    // устанавливает item активным в коллекции
    // ничего не делает, если item не принадлежит коллекции
    // краткое названия для читабельности. по сути setActiveInCollection
    void setActive(bool active);

    //---------------------------------------------------------------

    AnalyzerItem* propertyExtension() const
    {
        return _propertyExtension;
    }
    void setPropertyExtension(AnalyzerItem* extension);

    virtual void pushCommand(const AnalyzerCommand& command,
                             PushingType pushType = PushingType::defaultType);
    virtual void addCommandToQueue(const AnalyzerCommand& command,
                                   PushingType pushType = PushingType::defaultType);
    virtual QUuid uuid() const = 0;
    virtual QUuid propertyUuid(QString propertyName) const = 0;

    virtual AnalyzerWriteStream* streamWriter()
    {
        return _parentItem->streamWriter();
    }

    virtual StackUndoDispatcher* undoDispatcher()
    {
        return _parentItem->undoDispatcher();
    }

    virtual QQueue<AnalyzerCommand>* commandQueue()
    {
        return _parentItem->commandQueue();
    }

    // TODO: make protected -----------------------------

    void setCollection(AnalyzerItemCollectionBase* coll);

    void resetCollectionIndex(int index);
    void resetActiveState(bool isActive);

signals:
    void childAdded(AnalyzerItem* child);
    void beforeRemoveChild(AnalyzerItem* child);
    void childRemoved();
    //void childCountChanged(int count);

    void activeStateChanged(bool isActive);
    void collectionIndexChanged(int index);

    void analyzerItemPropertyAdded(); // cosmetic
    void uuidChanged();

    void propertyExtensionChanged(AnalyzerItem* extension);

public slots:
    virtual void onPropertyChangedUuid()
    {
        emit uuidChanged();
    }

    // TODO hide
    void addChild(AnalyzerItem* child);
    void removeChild(AnalyzerItem* child);

protected:
    //virtual void addDefaultChild() {}

    Q_INVOKABLE virtual void initUuid()
    {
        return;
    }

    void appendQueue(const AnalyzerCommand& command);
    void appendQueueFront(const AnalyzerCommand& command);
    void performQueueCommands();
    void performSingleCommand(const AnalyzerCommand& command);

    static QHash<QString, QUuid> generateStaticPropertiesUuidMap(QUuid parentUuid,
                                                                 QMetaObject metaObject);
    static QUuid generateStaticPropertyUuid(QHash<QString, QUuid> propertiesUuidMap,
                                            QString propertyName);

    QList<AnalyzerItem*> _childItems;
    AnalyzerItem* _parentItem;
    AnalyzerItem* _propertyExtension;

    AnalyzerItemCollectionBase* _containingCollection;
    int _collectionIndex;
    bool _isActive;

    //QQueue<AnalyzerCommand>* _queueCommandPtr;

    AnalyzerWriteStream* _streamWriter;
    //* _undoDispatcherPtr;

private:
    friend void deleteCommandFromQueue(AnalyzerItem* analyzerObject, const AnalyzerCommand& command);
};

} //namespace Planar



