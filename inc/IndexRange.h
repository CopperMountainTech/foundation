#pragma once
#include "FoundationGlobal.h"
#include "IndexSegment.h"

namespace Planar {

///                                      --->                 --->            --->
/// \brief Поддиапазон данных (....[start....stop]....)  |  (....stop]....[start....)
/// В отличае от IndexSegment, может зацикливаться, т.е. начинаться в правой части исходного диапазона, а
/// заканчиваться слева.
/// Фактически состоит из нескольких IndexSegment: из 2-х, если правая граница достигла конца
/// исходного диапазона; из 1-го, если правая граница не достигла конца исходного диапазона. Пустой
/// IndexRange не содержит ни одного сегмента.
///
class FOUNDATIONSHARED_EXPORT IndexRange
{
public:
    ///
    /// \brief Конструктор без инициализации - пустой IndexRange (length()==0, begin()==-1)
    /// \param[in] size размер исходного диапазона, максимально возможный размер IndexRange
    ///
    IndexRange(const int size = 1);
    ///
    /// \brief Конструктор
    /// \param[in] size размер исходного диапазона, максимально возможный размер IndexRange
    /// \param[in] begin начальный индекс IndexRange
    /// \param[in] length количество элементов в IndexRange
    ///
    IndexRange(const int size, const int begin, const int length);
    ///
    /// \brief Конструктор копий
    /// \param[in] other другой IndexRange
    ///
    IndexRange(const IndexRange& other);
    ///
    /// \brief Конструктор из IndexSegment
    /// \param[in] segment IndexSegment
    ///
    IndexRange(const IndexSegment& segment);

    ///
    /// \brief Геттер size
    /// \return размер исходного диапазона, максимально возможный размер IndexRange
    ///
    int size() const;
    ///
    /// \brief Геттер начального индекса IndexRange
    /// \return начальное значение IndexRange (-1 если количество элементов равно 0)
    ///
    int begin() const;
    ///
    /// \brief Геттер количества элементов в IndexRange
    /// \return количество элементов в IndexRange
    ///
    int length() const;

    ///
    /// \brief Сеттер IndexRange
    /// \param[in] begin начальное значение IndexRange
    /// \param[in] length количество значений в IndexRange
    ///
    void assign(const int begin, const int length);
    ///
    /// \brief Сбрасывает IndexRange к начальному состоянию (length()==0, begin()=-1)
    ///
    void clear();

    IndexRange& operator=(const IndexRange& other);
    IndexRange& operator+=(const IndexRange& other);
    IndexRange& operator-=(const IndexRange& other);
    IndexRange& operator=(const IndexSegment& segment);
    IndexRange& operator+=(const IndexSegment& segment);
    IndexRange& operator-=(const IndexSegment& segment);

    const IndexRange operator+(const IndexRange& other) const;
    const IndexRange operator-(const IndexRange& other) const;
    const IndexRange operator+(const IndexSegment& segment) const;
    const IndexRange operator-(const IndexSegment& segment) const;

    ///
    /// \brief Проверка последовательности IndexRange
    /// \param[in] other другой IndexRange
    /// \return истина, если текущий IndexRange следует сразу за другим с учётом цикличности
    ///
    bool isNextTo(const IndexRange& other) const;
    ///
    /// \brief Проверка на максимальный размер IndexRange
    /// \return истина, если IndexRange достиг максимального размера
    ///
    bool isFull() const;
    ///
    /// \brief Проверка на пустой IndexRange
    /// \return истина, если размер IndexRange равен 0
    ///
    bool isEmpty() const;
    ///
    /// \brief Проверка на сегментированность
    /// \return true, если IndexRange состоит из 2-х сегментов
    ///
    bool isSegmented() const;

    ///
    /// \brief Геттер сегметонов
    /// \return список сегментов. для пустого IndexRange - список пустой. для сегментированного IndexRange
    /// список будет сотоять из двух сегментов. для несегментированного - из одного.
    ///
    QList<IndexSegment> segments() const;

private:
    QList<IndexSegment> _segments;
    int _size;
};

} // namespace Planar

Q_DECLARE_METATYPE(Planar::IndexRange)
