#ifndef ANALYZERCOMMAND_H
#define ANALYZERCOMMAND_H

#include <QObject>

#include <FoundationGlobal.h>

namespace Planar {

class AnalyzerItem;
class AnalyzerPropertyBase;

class FOUNDATIONSHARED_EXPORT AnalyzerCommand
{
public:
    AnalyzerCommand();
    AnalyzerCommand(QPointer<AnalyzerPropertyBase> ptr, QMetaMethod method, QVariant value,
                    CommandType commandType = CommandType::defaultType,
                    CommandStatus commandStatus = CommandStatus::defaultStatus);

    AnalyzerCommand(const AnalyzerCommand& comm);
    AnalyzerCommand(const AnalyzerCommand& comm, const QVariant value,
                    bool checkPrevRequestRequired = true);

    bool operator== (const AnalyzerCommand& rhs);
    bool operator!= (const AnalyzerCommand& rhs);

    bool isPointerActual() const;
    QMetaMethod method() const;
    QVariant value() const;
    CommandType commandType() const;

    CommandStatus commandStatus() const;
    void setCommandStatus(CommandStatus status);

    long commandNumber() const;

    QPointer<AnalyzerPropertyBase>& propertyPointer();
    const QPointer<AnalyzerPropertyBase>& propertyPointer() const;

    bool checkPrevRequestRequired() const
    {
        return _checkPrevRequestRequired;
    }

private:
    void setValue(const QVariant value)
    {
        _value = value;
    }
    void setCommandNumber(const long number)
    {
        _commandNumber = number;
    }

    QMetaMethod _method;
    QVariant _value;

    CommandType _commandType;
    CommandStatus _commandStatus;
    long _commandNumber;

    //указатель на проперти, которая создала эту команду
    QPointer<AnalyzerPropertyBase> _propertyPointer;
    bool _checkPrevRequestRequired;

    friend class UserUndoStack;
    friend class StackUndo;
    friend class AnalyzerItem;
};

} //namespace Planar

#endif // ANALYZERCOMMAND_H
