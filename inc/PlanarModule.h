#pragma once
#include "FoundationGlobal.h"
#include <QObject>
#include <QThread>

namespace Planar {

class FOUNDATIONSHARED_EXPORT PlanarModule: public QObject
{
    Q_OBJECT

public:
    PlanarModule(QObject* parent = nullptr) : QObject(parent), _needToStop(false), _started(false)
    {
    }
    virtual ~PlanarModule()
    {
    }

    void runMultiThread(QThread* thread, const QString& threadName,
                        QThread::Priority priority = QThread::InheritPriority)
    {
        thread->setObjectName(threadName);
        this->moveToThread(thread);
        connect(thread, &QThread::finished, this, &PlanarModule::deleteLater);
        connect(thread, &QThread::finished, thread, &QThread::deleteLater);
        thread->start(priority);
    }
protected:
    bool _needToStop;
    bool _started;

public slots:
    virtual void stop()
    {
        _needToStop = true;
        _started = false;
    }
    virtual void start()
    {
        _needToStop = false;
    }
signals:
    void finished();
    void started();
    void debug(QString);
    void error(QString);

};

}

