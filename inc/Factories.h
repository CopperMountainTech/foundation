#ifndef FACTORIES_H
#define FACTORIES_H

#include <QMetaObject>
#include <QGenericArgument>
#include <QMap>

/*!
  \macro __INDIRECT_EXPAND__

  Макрос исправляющий неправильное поведение msbuild при вложенном вызове макросов
  очередной привет от microsoft

  \sa {microsoft} {sucks}
 */
#define __INDIRECT_EXPAND__(m, args) m args
/*!
  \macro FACTORY_BASE(KeyType, BaseClassName)
  \relates Foundation

  Макрос для создания базового класса реализующего фабрику наследников
  с возможностью создания экземпляра наследкика по ключу.

  \a KeyType определяет тип ключа по которому идентифицируются наследники
             это может быть QString или QUuid
  \a BaseClassName имя базового класса в который добавляется функционал фабрики

  \sa {Factory}, {Foundation}, FACTORY_DERIVED_H, FACTORY_DERIVED_CPP
*/
#define STATIC_FACTORY_BASE(KeyType, BaseClassName)\
    public:                                                             \
    /* получить список всех ключей доступных наследников */             \
    static QList<KeyType> variants(){ return hash().keys(); }           \
    /* таблица фабрики */                                               \
    static QMap<KeyType, const QMetaObject*> & hash() {                \
        static QMap<KeyType, const QMetaObject*> hash;                 \
        return hash;                                                    \
    }                                                                   \
    /* фабричный метод создания экземпляра наследника по ключу */       \
    static BaseClassName * create(KeyType name,                         \
            QGenericArgument val0 = QGenericArgument(nullptr),          \
            QGenericArgument val1 = QGenericArgument(),                 \
            QGenericArgument val2 = QGenericArgument(),                 \
            QGenericArgument val3 = QGenericArgument(),                 \
            QGenericArgument val4 = QGenericArgument(),                 \
            QGenericArgument val5 = QGenericArgument(),                 \
            QGenericArgument val6 = QGenericArgument(),                 \
            QGenericArgument val7 = QGenericArgument(),                 \
            QGenericArgument val8 = QGenericArgument(),                 \
            QGenericArgument val9 = QGenericArgument()){                \
        auto qMetaObject = hash()[name];                                \
        auto qObjectInstance = qMetaObject->newInstance(                \
            val0, val1, val2, val3, val4, val5, val6, val7, val8, val9);\
        return (BaseClassName *)(qObjectInstance);                      \
    }                                                                   \
    /* вспомогательная структура для регистрации наследников */         \
    struct BaseMapEntry {                                               \
        explicit BaseMapEntry(KeyType name, const QMetaObject* object) {\
            BaseClassName::hash()[name] = object;                       \
    }};
#define __FACTORY_BASE_CONSTRUCT_INVOKE__(                              \
    _1, _2, _3, _4, _5, _6, _7, _8, NAME, ...) NAME
#define __FACTORY_BASE_INVOKE_DUMMY__(key, ...) nullptr
#define __FACTORY_BASE_INVOKE2__(key, type1, value1)                    \
    create(key, Q_ARG(type1, value1))
#define __FACTORY_BASE_INVOKE4__(key, type1, value1, type2, value2)     \
    create(key, Q_ARG(type1, value1),                                   \
                Q_ARG(type2, value2))
#define __FACTORY_BASE_INVOKE6__(key, type1, value1, type2, value2,     \
                                      type3, value3)                    \
    create(key, Q_ARG(type1, value1),                                   \
                Q_ARG(type2, value2),                                   \
                Q_ARG(type3, value3))
#define __FACTORY_BASE_INVOKE8__(key, type1, value1, type2, value2,     \
                                      type3, value3, type4, value4)     \
    create(key, Q_ARG(type1, value1), Q_ARG(type2, value2),             \
                Q_ARG(type3, value3), Q_ARG(type4, value4))
#ifdef _MSC_VER
#  define __FACTORY_BASE_INVOKE__(key, ...)                             \
    __INDIRECT_EXPAND__(__FACTORY_BASE_CONSTRUCT_INVOKE__,(__VA_ARGS__, \
        __FACTORY_BASE_INVOKE8__,                                       \
        __FACTORY_BASE_INVOKE_DUMMY__,                                  \
        __FACTORY_BASE_INVOKE6__,                                       \
        __FACTORY_BASE_INVOKE_DUMMY__,                                  \
        __FACTORY_BASE_INVOKE4__,                                       \
        __FACTORY_BASE_INVOKE_DUMMY__,                                  \
        __FACTORY_BASE_INVOKE2__,                                       \
        __FACTORY_BASE_INVOKE_DUMMY__))(key, __VA_ARGS__)
#else
#  define __FACTORY_BASE_INVOKE__(key, ...)                             \
    __FACTORY_BASE_CONSTRUCT_INVOKE__(__VA_ARGS__,                      \
        __FACTORY_BASE_INVOKE8__,                                       \
        __FACTORY_BASE_INVOKE_DUMMY__,                                  \
        __FACTORY_BASE_INVOKE6__,                                       \
        __FACTORY_BASE_INVOKE_DUMMY__,                                  \
        __FACTORY_BASE_INVOKE4__,                                       \
        __FACTORY_BASE_INVOKE_DUMMY__,                                  \
        __FACTORY_BASE_INVOKE2__,                                       \
        __FACTORY_BASE_INVOKE_DUMMY__)(key, __VA_ARGS__)
#endif

#define FACTORY_BASE(KeyType, BaseClassName, ...)                       \
    STATIC_FACTORY_BASE(KeyType, BaseClassName)                         \
    /* текущее значение ключа */                                        \
    Q_PROPERTY(KeyType key READ key WRITE setKey NOTIFY keyChanged)     \
    /* список ключей всех доступных реализаций */                       \
    Q_PROPERTY(QList<KeyType> keys READ keys)/*TODO keysChanged)*/      \
    /* экземпляр текущей реализации */                                  \
    Q_PROPERTY(BaseClassName* instance READ instance NOTIFY keyChanged DESIGNABLE false) \
    public:                                                             \
    /* экземпляр текущей реализации */                                  \
    virtual BaseClassName* instance() {                                 \
        if(!keys().contains(_key))\
            _key = keys().at(0);\
        if(_instances.contains(_key)) return _instances.value(_key);    \
        auto newInstance = __INDIRECT_EXPAND__(                         \
            __FACTORY_BASE_INVOKE__, (_key, __VA_ARGS__));              \
        _instances[_key] = newInstance;                                 \
        return newInstance; }                                           \
    /* установить текущее значение ключа */                             \
    void setKey(KeyType key) {                                          \
        if(key == _key) return;                                         \
        if(!keys().contains(key))                                       \
            key = keys().at(0);                                         \
        _key = key;                                                     \
        if(!_instances.contains(_key)) {                                \
            auto newInstance = __INDIRECT_EXPAND__(                     \
                __FACTORY_BASE_INVOKE__, (_key, __VA_ARGS__));          \
            _instances[_key] = newInstance; \
            /*connect(this, &BaseClassName::keyChanged, newInstance, &BaseClassName::activated);*/\
        } \
        _instances[_key]->activated();\
        emit keyChanged(); }                                            \
    /* прочитать текущее значение ключа */                              \
    KeyType key() { return _key; }                                      \
    /* получить список ключей всех реализаций */                        \
    QList<KeyType> keys() { return BaseClassName::variants();}          \
    /* сигнал изменения текущего ключа*/                                \
    Q_SIGNAL void keyChanged();                                         \
    Q_SLOT virtual void activated() {}                                   \
    private:                                                            \
        QMap<KeyType, BaseClassName*> _instances;                       \
        KeyType _key;

/*!
  \macro FACTORY_DERIVED_H
  \relates Foundation

  Макрос декларации наследника фабрики создающей наследников по ключу
  Используется в заголовочном файле.

  \sa {Factory}, {Foundation}, FACTORY_BASE, FACTORY_DERIVED_CPP, QMap
  */
#define FACTORY_DERIVED_H(BaseClassName, DerivedClassName) \
    private: static BaseMapEntry entry; \
    public: BaseClassName* instance() override { return nullptr; }
/*!
  \macro MULTIKEY_FACTORY_DERIVED_H(name)
  \relates Foundation

  Макрос декларации наследника фабрики создающей наследников по нескольким ключам
  Используется в заголовочном файле.
  Может декларироваться несколько раз в зависимости от количества значений ключа с
  которым необходимо ассоциировать данную реализацию.

  \a name Уникальное значение ассоциированное с ключом

  \sa {Factory}, {Foundation}, FACTORY_BASE, FACTORY_DERIVED_CPP, QMap
  */
#define MULTIKEY_FACTORY_DERIVED_H(name) \
    private: static BaseMapEntry entry##name;

/*!
  \macro FACTORY_DERIVED_CPP(Key, BaseClassName, DerivedClassName)
  \relates Foundation

  Макрос декларации наследника фабрики создающей наследников по ключу
  Используется в исходном файле. Необходим для регистрации класса в статической таблице фабрикию

  \a Key Значение ключа которое будет ассоциировано с типом регистрируемого наследника
  \a BaseClassName Тип базового класса в котором регистрируется наследник
  \a DerivedClassName Тип регистрируемого класса наследника

  \sa {Factory}, {Foundation}, FACTORY_BASE, FACTORY_DERIVED_H
  */
#define FACTORY_DERIVED_CPP(Key, BaseClassName, DerivedClassName) \
    BaseClassName::BaseMapEntry DerivedClassName::entry(          \
            Key, &DerivedClassName::staticMetaObject);
/*!
  \macro MULTIKEY_FACTORY_DERIVED_CPP(Key, BaseClassName, DerivedClassName, name)
  \relates Foundation

  Макрос декларации наследника фабрики создающей наследников по ключу
  Используется в исходном файле. Необходим для регистрации класса в статической таблице фабрикию

  \a Key Значение ключа которое будет ассоциировано с типом регистрируемого наследника
  \a BaseClassName Тип базового класса в котором регистрируется наследник
  \a DerivedClassName Тип регистрируемого класса наследника
  \a name Уникальное значение ассоциированное с ключом, совпадает с используемом в MULTIKEY_FACTORY_DERIVED_H

  \sa {Factory}, {Foundation}, FACTORY_BASE, MULTIKEY_FACTORY_DERIVED_H
  */
#define MULTIKEY_FACTORY_DERIVED_CPP(Key, BaseClassName, DerivedClassName, name) \
    BaseClassName::BaseMapEntry DerivedClassName::entry##name(                   \
            Key, &DerivedClassName::staticMetaObject);

#endif // FACTORIES_H
