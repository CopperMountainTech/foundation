#pragma once
#include "FoundationGlobal.h"

namespace Planar {


#define SAFE_FREE(ptr) \
        delete ptr; \
        ptr = nullptr;

const std::size_t RAWDATA_CHUNK_LENGTH = 16384;
const std::size_t SAMPLES_IN_CHUNK = RAWDATA_CHUNK_LENGTH / 2 - 8;


}

