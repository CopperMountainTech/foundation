#ifndef SERIALIZEDUNIT_H
#define SERIALIZEDUNIT_H

#include <Units.h>
#include <QXmlStreamWriter>

namespace Planar {

class FOUNDATIONSHARED_EXPORT SerializedUnit : public Unit
{
public:
    SerializedUnit() : Unit() {}
    SerializedUnit(const Unit& prototype) : Unit(prototype) {}

    // создает объект со спецификацией и множителем прототипа, но с произвольным значением value
    SerializedUnit(double value, const Unit& prototype) : Unit(value, prototype) {}

    SerializedUnit(const UnitSpecification& specification, double value, double multiplier) :
        Unit(specification, value, multiplier) {}

    void operator >> (QXmlStreamWriter* stream)
    {
        stream->writeAttribute("Value", QString::number(this->value()));
        stream->writeAttribute("Multiplier", QString::number(this->multiplier()));
        stream->writeAttribute("UnitSpecification",
                               QString::number(reinterpret_cast<unsigned long long>(_specification)));
    }
};

} //namespace Planar

#endif // SERIALIZEDUNIT_H
