#ifndef FACTRORYBASE_H
#define FACTRORYBASE_H

#include <FoundationGlobal.h>
#include <QList>


template<typename KeyType>
class FactoryBase
{
public:
    virtual ~FactoryBase() {}

    virtual KeyType key() = 0;
    virtual void setKey(KeyType key) = 0;
    virtual QList<KeyType> keys() = 0;
};

#endif // FACTRORYBASE_H
