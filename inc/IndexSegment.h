#pragma once
#include "FoundationGlobal.h"

namespace Planar {

///                                              --->
/// \brief Непрерывный сегмент данных (....[begin....end]....)
/// Добавлять к сегменту можно справа, вычитать - слева.
/// Складывать и вычитать можно только сегменты с одинаковым размером исходного диапазона(size).
/// У пустого сегмента количество элементов равно 0, а индекс начала равен -1.
///
class FOUNDATIONSHARED_EXPORT IndexSegment
{
public:
    ///
    /// \brief Конструктор без инициализации - пустой сегмент: length()==0, begin()==-1
    /// \param[in] size размер исходного диапазона, максимально возможный размер IndexSegment
    ///
    IndexSegment(const int size);
    ///
    /// \brief Конструктор
    /// \param[in] size размер исходного диапазона, максимально возможный размер IndexSegment
    /// \param[in] begin начальный индекс IndexSegment
    /// \param[in] length количество элементов в IndexSegment
    ///
    IndexSegment(const int size, const int begin, const int length);
    ///
    /// \brief Конструктор копий
    /// \param[in] other другой IndexSegment
    ///
    IndexSegment(const IndexSegment& other);

    ///
    /// \brief Геттер size
    /// \return размер исходного диапазона, максимально возможный размер IndexSegment
    ///
    int size() const;
    ///
    /// \brief Геттер начального индекса IndexSegment
    /// \return начальное значение IndexSegment (-1 если количество элементов равно 0)
    ///
    int begin() const;
    ///
    /// \brief Геттер конечного индекса IndexSegmant
    /// \return конечное значение IndexSegment (-1 если количество элементов равно 0)
    ///
    int end() const;
    ///
    /// \brief Геттер количества элементов в IndexSegment
    /// \return количество элементов в IndexSegment
    ///
    int length() const;

    ///
    /// \brief Сеттер IndexSegment
    /// \param[in] begin начальное значение IndexSegment
    /// \param[in] length количество значений в IndexSegment
    ///
    void assign(const int begin, const int length);
    ///
    /// \brief Сбрасывает IndexSegment к начальному(пустому) состоянию (length()==0, begin()=-1)
    ///
    void clear();
    ///
    /// \brief Сбрасывает IndexSegment к начальному состоянию с заданием исходного диапазона
    /// \param[in] size размер исходного диапазона, максимально возможный размер IndexSegment
    ///
    void reset(const int size);

    IndexSegment& operator=(const IndexSegment& other);
    IndexSegment& operator+=(const IndexSegment& other);
    IndexSegment& operator-=(const IndexSegment& other);

    const IndexSegment operator+(const IndexSegment& other) const;
    const IndexSegment operator-(const IndexSegment& other) const;

    ///
    /// \brief Проверка последовательности IndexSegment
    /// \param[in] other другой IndexSegment
    /// \return истина, если текущий IndexSegment следует сразу за другим
    ///
    bool isNextTo(const IndexSegment& other) const;
    ///
    /// \brief Проверка на максимальный размер IndexSegment
    /// \return истина, если IndexSegment достиг максимального размера
    ///
    bool isFull() const;
    ///
    /// \brief Проверка на пустой IndexSegment
    /// \return истина, если размер IndexSegment равен 0
    ///
    bool isEmpty() const;

    ///
    /// \brief Проверка на расширяемость
    /// \return истина, если правая граница сегмента не достигла конца
    ///
    bool isExtendable() const;

private:
    int _size;
    int _begin;
    int _length;
};

} // namespace Planar
