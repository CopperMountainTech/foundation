#ifndef BOOLPROPERTY_H
#define BOOLPROPERTY_H

#include "AnalyzerPropertyBase.h"
#include <FoundationGlobal.h>

namespace Planar {

class FOUNDATIONSHARED_EXPORT BoolProperty : public AnalyzerPropertyBase
{
    Q_OBJECT
    Q_PROPERTY(bool value READ value WRITE requestValue NOTIFY valueChanged)

public:
    BoolProperty(AnalyzerItem* parent, bool value,
                 CommandType commandType = CommandType::defaultType);

    bool value() const;

    //метод accept устанавливает значения без постановки в очередь
    // !!! не использовать для вызова из UI !!!
    void acceptValue(bool value);

public slots:
    void requestValue(bool value);

signals:
    void approvalRequired(bool& result, bool& isApproved);
    void valueChanged(bool value);

private:
    Q_INVOKABLE void setValueHandler(QVariant value) override;
    bool _value;
};

} //namespace Planar

#endif // BOOLPROPERTY_H
