#ifndef FACTORYPROPERTY_H
#define FACTORYPROPERTY_H

#include "AnalyzerPropertyBase.h"
#include "BiMap.h"
#include "FactoryBase.h"
#include <FoundationGlobal.h>
#include <QCoreApplication>

namespace Planar {

class FOUNDATIONSHARED_EXPORT FactoryPropertyBase : public AnalyzerPropertyBase
{
    Q_OBJECT
    Q_PROPERTY(QVariant value READ qvariantValue WRITE requestValue NOTIFY valueChanged)
    Q_PROPERTY(QStringList listValue READ listValue NOTIFY listValueChanged)
    Q_PROPERTY(int index READ index WRITE requestIndex NOTIFY indexChanged)

public:
    explicit FactoryPropertyBase(AnalyzerItem* parent);
    int index() const;

protected:
    virtual void requestValue(QVariant value) = 0;
    virtual QStringList listValue() = 0;
    virtual QVariant qvariantValue() = 0;
    virtual void requestIndex(int index) = 0;

    void saveLastCondition(QVariant value, const int id) override;
    virtual void acceptValueVariant(QVariant valueVariant) = 0;
    void acceptedIndex(int index);

    Q_INVOKABLE void setValueHandler(QVariant value) override;

signals:
    void approvalRequired(QVariant& result, bool& isApproved);
    void valueChanged();
    void listValueChanged();
    void indexChanged(int index);

protected:
    int _index;
};

//TODO: проверить на мьютексы!

template<typename KeyType>
class FactoryProperty : public FactoryPropertyBase
{
public:
    FactoryProperty(AnalyzerItem* parent,
                    FactoryBase<KeyType>* factoryBase,
                    const BiMap<KeyType, QString>& biMap);

    KeyType value() const;
    void requestValue(KeyType value);
    void requestValue(QVariant value) override;
    void acceptValue(const KeyType& value);
    QVariant qvariantValue() override;
    void acceptListValue(QList<KeyType> listValue);
    QStringList listValue() override;
    virtual void requestIndex(int index);

protected:
    void acceptValueVariant(QVariant valueVariant) override final;

private:
    FactoryBase<KeyType>* _factoryBase;
    KeyType _value;
    KeyType _prevRequestValue; //переменная для хранения предыдущего состояния запроса
    QList<KeyType> _listValue;
    const BiMap<KeyType, QString>& _biMap;
};


template<typename KeyType>
FactoryProperty<KeyType>::FactoryProperty(AnalyzerItem* parent,
                                          FactoryBase<KeyType>* factoryBase,
                                          const BiMap<KeyType, QString>& biMap)

    : FactoryPropertyBase(parent), _factoryBase(factoryBase), _value(factoryBase->key()),
      _prevRequestValue(factoryBase->key()), _listValue(factoryBase->keys()),
      _biMap(biMap)
{
    _index = _listValue.indexOf(_value);
}


template<typename KeyType>
KeyType FactoryProperty<KeyType>::value() const
{
    QReadLocker locker(&_lock);
    return _value;
}


template<typename KeyType>
void FactoryProperty<KeyType>::requestValue(KeyType value)
{
    requestValue(QVariant::fromValue(value));
}


template<typename KeyType>
void FactoryProperty<KeyType>::requestValue(QVariant value)
{
    QReadLocker locker(&_lock);
    KeyType typeValue;

    if (value.canConvert<KeyType>() == false)
        return;

    typeValue = value.value<KeyType>();
    KeyType currentValue = _value;
    if (currentValue == typeValue)
        return;

    if (_prevRequestValue == typeValue)
        return;

    _prevRequestValue = typeValue;
    locker.unlock();
    createAndPushCommand(QVariant::fromValue(typeValue));
}


template<typename KeyType>
void FactoryProperty<KeyType>::acceptValue(const KeyType& value)
{
    _lock.lockForWrite();
    saveLastCondition(QVariant::fromValue(_value), id());
    _value = value;
    _prevRequestValue = value;

    int newIndex = _listValue.indexOf(value);
    if (newIndex != -1) {
        _factoryBase->setKey(value);
        _lock.unlock();
        acceptedIndex(newIndex);
    } else {
        _lock.unlock();
        qDebug() << "unacceptable value";
        Q_ASSERT(false);
    }
    emit valueChanged();
}


template<typename KeyType>
QVariant FactoryProperty<KeyType>::qvariantValue()
{
    return QVariant::fromValue(_value);
}


template<typename KeyType>
void FactoryProperty<KeyType>::acceptListValue(QList<KeyType> listValue)
{
    _lock.lockForWrite();
    _listValue = listValue;
    _lock.unlock();
    emit listValueChanged();
}


template<typename KeyType>
QStringList FactoryProperty<KeyType>::listValue()
{
    QWriteLocker locker(&_lock);
    QStringList stringList;
    foreach (KeyType t, _listValue) {
        QVariant p = QVariant::fromValue(t);
        QUuid uuid = p.value<QUuid>();
        QString translated = _biMap.value2(uuid);
        stringList.append(translated);
    }
    return stringList;
}


template<typename KeyType>
void FactoryProperty<KeyType>::requestIndex(int index)
{
    QReadLocker locker(&_lock);
    if (_index == index)
        return;

    if ( (index < 0) || (index >= _listValue.count()) ) {
        qDebug() << "unacceptable index";
        Q_ASSERT(false);
        return;
    }

    locker.unlock();
    //восстанавливаем значение в конроле, т.к. flux!
    emit indexChanged(_index);
    requestValue(_listValue.at(index));
}

template<typename KeyType>
void FactoryProperty<KeyType>::acceptValueVariant(QVariant valueVariant)
{
    KeyType value = valueVariant.value<KeyType>();
    acceptValue(value);
}


} //namespace Planar

#endif // FACTORYPROPERTY_H
