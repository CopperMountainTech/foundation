#ifndef ANALYZERPROPERTYBASE_H
#define ANALYZERPROPERTYBASE_H

#include <QMutex>
#include <QStack>
#include <QReadWriteLock>

#include "PropertyBase.h"
#include <FoundationGlobal.h>
#include "UUidCommon.h"

#define PROPERTY_UUID(ns)                              \
    public:                                                 \
    static QUuid staticUuid() {                             \
        static QUuid uuid = QUuid::createUuidV5(            \
                    QUuid(ns),                              \
                    QString(staticMetaObject.className())); \
        return uuid; }                                      \
    QUuid uuid() const override { return staticUuid(); }

namespace Planar {

class AnalyzerItem;

class FOUNDATIONSHARED_EXPORT AnalyzerPropertyBase : public PropertyBase
{
    Q_OBJECT
    //ANALYZER_ITEM_UUID(ANALYZER_UUID)
public:
    AnalyzerPropertyBase(AnalyzerItem* parent,
                         CommandType commandType = CommandType::defaultType,
                         bool packingType = false);

    //QUuid uuid();

    bool packingType() const;
    const QVariant* currentRequestedValue() const;
    QVariant extractCurrentRequestedValue();
    virtual QVariant valueAsVariant() const;
    void resetCurrentRequestedValue(const QVariant value);
    QPointer<AnalyzerItem>* parentPointer();

    void setIdHandler(const int handlerId);

protected:
#ifdef STACKUNDO
    void createAndPushCommand(QVariant value, QVariant lastValue);
#else
    void createAndPushCommand(QVariant value);
#endif//STACKUNDO
    int id() const;
    int idMax() const;
    int idMin() const;
    virtual void saveLastCondition(QVariant value, const int id);

    void setIdMax(const int handlerId);
    void setIdMin(const int handlerId);
    bool isNewCommandNeeded(const QVariant& newValue);
    virtual bool isValuesEqual(const QVariant& prevValue, const QVariant& value) const;

    //these methods are needed to invoke from Undo stack
    virtual void acceptMinHandler(QVariant min);
    virtual void acceptMaxHandler(QVariant max);

    virtual void setValueHandler(QVariant value) = 0;
    virtual void defaultApprove(QVariant& result, bool& isApproved);

    AnalyzerItem* _parent;
    QPointer<AnalyzerItem> _parentQPointer;
    //TODO: проверить использование пар -ров и убрать лишние
    QVariant _currentRequestedValue;      //previous value of *_valuePtr
    QVariant _valueAsVariant;  //current _value in descendant property as Variant
    mutable QReadWriteLock _lock;

private:
    int _id;
    int _idMax;
    int _idMin;
    //QUuid _uuid;
    CommandType _commandType;

    //possibility of packing. _packingType == Enabled may be packed to one command object in Undo stack
    bool _packingType;
};

inline int AnalyzerPropertyBase::id() const
{
    return _id;
}

} //namespace Planar

#endif // ANALYZERPROPERTYBASE_H
