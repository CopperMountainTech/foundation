#ifndef ENUMPROPERTY_H
#define ENUMPROPERTY_H

#include <QMetaObject>
#include <QMetaEnum>
#include <QtCore/QMutex>

#include "AnalyzerPropertyBase.h"
#include <FoundationGlobal.h>

namespace Planar {

class AnalyzerItem;

class FOUNDATIONSHARED_EXPORT EnumPropertyBase : public AnalyzerPropertyBase
{
    Q_OBJECT
    Q_PROPERTY(QVariant value READ valueAsVariant WRITE requestValue NOTIFY valueChanged)

public:
    explicit EnumPropertyBase(AnalyzerItem* parent);

protected:
    virtual void requestValue(QVariant value) = 0;
    virtual void acceptValueVariant(QVariant valueVariant) = 0;

    Q_INVOKABLE void setValueHandler(QVariant value) override
    {
        int result(value.value<int>());
        bool isApproved = true;
        static const QMetaMethod approveSignal = QMetaMethod::fromSignal(
                                                     &EnumPropertyBase::approvalRequired);
        if (isSignalConnected(approveSignal)) {
            emit approvalRequired(result, isApproved);
        } else {
            QVariant variantResult(QVariant::fromValue(result));
            defaultApprove(variantResult, isApproved);
            result = variantResult.value<int>();
        }

        if (isApproved)
            acceptValueVariant(result);
    }

signals:
    void valueChanged(int value);
    void approvalRequired(int& result, bool& isApproved);

private:
    void acceptMinHandler(QVariant min) override final
    {
        Q_UNUSED(min);
    }
    void acceptMaxHandler(QVariant max) override final
    {
        Q_UNUSED(max);
    }
};


template<typename EnumType>
class EnumProperty : public EnumPropertyBase
{
public:
    EnumProperty(AnalyzerItem* parent, const EnumType& value)
        : EnumPropertyBase(parent), _value(value)
    {
        _currentRequestedValue = QVariant::fromValue(_value);
        _valueAsVariant = _currentRequestedValue;
    }

    EnumType value() const
    {
        QReadLocker locker(&_lock);
        return _value;
    }

    //метод accept устанавливает значения без постановки в очередь
    // !!! не использовать для вызова из UI !!!
    void acceptValue(const EnumType& value)
    {
        _lock.lockForWrite();
        if (_value == value) {
            _lock.unlock();
            return;
        }

        saveLastCondition(static_cast<int>(_value), id());

        _value = value;
        resetCurrentRequestedValue(QVariant::fromValue(_value));
        _lock.unlock();

        emit valueChanged(static_cast<int>(_value));
    }

    void requestValue(EnumType value)
    {
        createAndPushCommand(QVariant::fromValue(value));
    }

protected:
    void acceptValueVariant(QVariant valueVariant) override final
    {
        EnumType value = valueVariant.value<EnumType>();
        acceptValue(value);
    }

    void requestValue(QVariant value) override
    {
        QReadLocker locker(&_lock);
        bool ok;
        int intValue = value.toInt(&ok);
        Q_ASSERT(ok == true);

        int currentValue = static_cast<int>(_value);
        if (currentValue == intValue)
            return;

        if (!isNewCommandNeeded(value))
            return;

        EnumType enumValue = static_cast<EnumType>(intValue);
        locker.unlock();
        createAndPushCommand(QVariant::fromValue(enumValue));
    }

private:
    EnumType _value;
};

} //namespace Planar

#endif // ENUMPROPERTY_H
