#ifndef COMMANDPROPERTY_H
#define COMMANDPROPERTY_H

#include "AnalyzerPropertyBase.h"
#include <FoundationGlobal.h>

namespace Planar {

class AnalyzerItem;

class FOUNDATIONSHARED_EXPORT CommandProperty : public AnalyzerPropertyBase
{
    Q_OBJECT

public:
    CommandProperty(AnalyzerItem* parent,
                    CommandType commandType = CommandType::defaultType);
signals:
    void handlerIsInvoked(QVariant value);

public slots:
    void makeRequest(QVariant value = 0);

private:
    Q_INVOKABLE void setValueHandler(QVariant value) override;

};

} //namespace Planar

#endif // COMMANDPROPERTY_H
