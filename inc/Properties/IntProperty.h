#ifndef INTPROPERTY_H
#define INTPROPERTY_H

#include <QMutex>

#include "AnalyzerPropertyBase.h"
#include <FoundationGlobal.h>

namespace Planar {

class AnalyzerItem;

class FOUNDATIONSHARED_EXPORT IntProperty : public AnalyzerPropertyBase
{
    Q_OBJECT
    Q_PROPERTY(int value READ value WRITE requestValue NOTIFY valueChanged)
    Q_PROPERTY(int min READ min NOTIFY minChanged)
    Q_PROPERTY(int max READ max NOTIFY maxChanged)

    static const int IntPropertyMax = 200000;

public:
    IntProperty(AnalyzerItem* parent,
                int value,
                int min = 0,
                int max = IntPropertyMax,
                CommandType commandType = CommandType::defaultType);
    int value() const;
    int min() const;
    int max() const;

    //методы accept устанавливают значения без постановки в очередь
    // !!! не использовать для вызова из UI !!!
    void acceptValue(int value, bool forceEmitSignal = false);
    void acceptMin(int min);
    void acceptMax(int max);

public slots:
    void requestValue(int value);

signals:
    void approvalRequired(int& result, bool& isApproved);
    void valueChanged(int value);
    void minChanged(int min);
    void maxChanged(int max);

private:
    //методы нужны для вызова из стэка Undo
    Q_INVOKABLE void acceptMinHandler(QVariant min) override;
    Q_INVOKABLE void acceptMaxHandler(QVariant max) override;
    Q_INVOKABLE void setValueHandler(QVariant value) override;
    void defaultApprove(QVariant& result, bool& isApproved) override;
    void checkMinMax(int& value);

    int _value;
    int _min;
    int _max;
};

} //namespace Planar

#endif // INTPROPERTY_H
