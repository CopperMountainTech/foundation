#ifndef UnitProperty_H
#define UnitProperty_H

#include <QtCore/QMutex>

#include <Units.h>
#include <SerializedUnit.h>

#include "AnalyzerPropertyBase.h"
#include <FoundationGlobal.h>
#include <QQueue>
#include <QVariant>

namespace Planar {

class FOUNDATIONSHARED_EXPORT UnitProperty : public AnalyzerPropertyBase
{
    Q_OBJECT
    Q_PROPERTY(Planar::Unit value READ value WRITE requestValue NOTIFY valueChanged)
public:
    UnitProperty(AnalyzerItem* parent, const Planar::Unit& value, const Unit& min,
                 const Unit& max, CommandType commandType = CommandType::defaultType,
                 bool packingType = false);

    Planar::Unit value() const;
    Planar::Unit min() const;
    Planar::Unit max() const;

    //методы accept устанавливают значения без постановки в очередь
    // !!! не использовать для вызова из UI !!!
    void acceptValue(Planar::Unit value);
    void acceptMin(Planar::Unit min);
    void acceptMax(Planar::Unit max);

    void reset(Unit value, Unit min, Unit max);

public slots:
    void requestValue(Planar::Unit value);

signals:
    void approvalRequired(Planar::Unit& result,
                          bool& isApproved); //должен быть подключен к слоту только с DirectConnection!
    void valueChanged(Planar::Unit value);
    void minChanged(Planar::Unit min);
    void maxChanged(Planar::Unit max);

protected:
    bool isValuesEqual(const QVariant& prevValue, const QVariant& value) const override;

private:
    void saveLastCondition(QVariant value, const int id) override;
    void checkMinMax(Unit& value);

    //методы нужны для вызова из стэка Undo
    Q_INVOKABLE void acceptMinHandler(QVariant min) override;
    Q_INVOKABLE void acceptMaxHandler(QVariant max) override;
    Q_INVOKABLE void setValueHandler(QVariant value) override;
    void defaultApprove(QVariant& result, bool& isApproved) override;

    Planar::Unit _value;
    Planar::Unit _min;
    Planar::Unit _max;
};

} //namespace Planar

#endif // UnitProperty_H
