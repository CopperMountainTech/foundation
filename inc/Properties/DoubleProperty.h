#ifndef DOUBLEPROPERTY_H
#define DOUBLEPROPERTY_H

#include <QtCore/QMutex>

#include "Properties/AnalyzerPropertyBase.h"
#include <FoundationGlobal.h>

namespace Planar {

class DoubleProperty : public AnalyzerPropertyBase
{
    Q_OBJECT
    Q_PROPERTY(double value READ value WRITE requestValue NOTIFY valueChanged)
    Q_PROPERTY(double min READ min NOTIFY minChanged)
    Q_PROPERTY(double max READ max NOTIFY maxChanged)

public:
    DoubleProperty(AnalyzerItem* parent, double value,
                   CommandType commandType = CommandType::defaultType);
    double value() const;
    double min() const;
    double max() const;

    //методы accept устанавливают значения без постановки в очередь
    // !!! не использовать для вызова из UI !!!
    void acceptValue(double value);
    void acceptMin(double min);
    void acceptMax(double max);

public slots:
    void requestValue(double value);

signals:
    void approvalRequired(double& result, bool& isApproved);
    void valueChanged(double value);
    void minChanged(double min);
    void maxChanged(double max);

private:
    //методы нужны для вызова из стэка Undo
    Q_INVOKABLE void acceptMinHandler(QVariant min) override;
    Q_INVOKABLE void acceptMaxHandler(QVariant max) override;

    //внутренний хэндлер
    Q_INVOKABLE void setValueHandler(QVariant value) override;
    void defaultApprove(QVariant& result, bool& isApproved) override;
    void checkMinMax(double& value);

    double _value;//TODO: заменить на атомики
    double _min;
    double _max;
};

} //namespace Planar

#endif // DOUBLEPROPERTY_H
