#ifndef FABRICPROPERTY_H
#define FABRICPROPERTY_H

#include "AnalyzerPropertyBase.h"
#include <FoundationGlobal.h>

namespace Planar {

class FOUNDATIONSHARED_EXPORT FabricPropertyBase : public AnalyzerPropertyBase
{
    Q_OBJECT
    Q_PROPERTY(QVariant value READ valueAsVariant WRITE requestValue NOTIFY valueChanged)
    Q_PROPERTY(QStringList listValue READ listValue NOTIFY listValueChanged)

public:
    explicit FabricPropertyBase(AnalyzerItem* parent);

protected:
    virtual void requestValue(QVariant value) = 0;
    virtual QStringList listValue() = 0;

    void saveLastCondition(QVariant value, const int id) override;
    virtual void acceptValueVariant(QVariant valueVariant) = 0;

    Q_INVOKABLE void setValueHandler(QVariant value) override
    {
        QVariant result(value);
        bool isApproved = false;
        static const QMetaMethod approveSignal = QMetaMethod::fromSignal(
                                                     &FabricPropertyBase::approvalRequired);
        if (isSignalConnected(approveSignal)) {
            emit approvalRequired(result, isApproved);
        } else {
            defaultApprove(result, isApproved);
        }

        if (isApproved)
            acceptValueVariant(result);
    }

signals:
    void approvalRequired(QVariant& result, bool& isApproved);
    void valueChanged();
    void listValueChanged();
};

template<typename KeyType>
class FabricProperty : public FabricPropertyBase
{
public:
    FabricProperty(AnalyzerItem* parent, const KeyType& value)
        : FabricPropertyBase(parent), _value(value)
    {
        _currentRequestedValue = QVariant::fromValue(_value);
        _valueAsVariant = _currentRequestedValue;
    }

    KeyType value() const
    {
        QReadLocker locker(&_lock);
        return _value;
    }

    void requestValue(KeyType value)
    {
        requestValue(QVariant::fromValue(value));
    }

    void requestValue(QVariant value) override
    {
        QReadLocker locker(&_lock);
        KeyType typeValue;

        if (value.canConvert<KeyType>())
            typeValue = value.value<KeyType>();
        else
            return;

        KeyType currentValue = _value;
        if (currentValue == typeValue)
            return;

        if (!isNewCommandNeeded(QVariant::fromValue(value)))
            return;

        locker.unlock();
        createAndPushCommand(QVariant::fromValue(typeValue));
    }

    void acceptValue(const KeyType& value)
    {
        _lock.lockForWrite();
        if (_value == value) {
            _lock.unlock();
            return;
        }
        saveLastCondition(QVariant::fromValue(_value), id());
        _value = value;
        resetCurrentRequestedValue(QVariant::fromValue(_value));

        _lock.unlock();
        emit valueChanged();
    }

    void acceptListValue(QList<KeyType> listValue)
    {
        _lock.lockForWrite();
        _listValue = listValue;
        _lock.unlock();
        emit listValueChanged();
    }

    QStringList listValue() override
    {
        _lock.lockForWrite();
        QStringList stringList;
        foreach (KeyType t, _listValue) {
            QVariant p = QVariant::fromValue(t);
            stringList.append(p.value<QString>());
        }
        _lock.unlock();
        return stringList;
    }

protected:
    void acceptValueVariant(QVariant valueVariant) override final
    {
        KeyType value = valueVariant.value<KeyType>();
        acceptValue(value);
    }

private:
    KeyType _value;
    QList<KeyType> _listValue;
};

} //namespace Planar

#endif // FABRICPROPERTY_H
