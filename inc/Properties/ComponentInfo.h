#pragma once

#include <QObject>
#include <QString>
#include <QUuid>

namespace Planar {

class ComponentInfo
{
public:
    ComponentInfo(QString frame, QUuid uuid, QString delegate, QString position, QString extProperty, QObject* customDelegate = nullptr):
        _frame(frame),
        _uuid(uuid),
        _delegate(delegate),
        _position(position),
        _extProperty(extProperty),
        _customDelegate(customDelegate)
    {
    }

    QString frame()
    {
        return _frame;
    }

    QUuid uuid()
    {
        return _uuid;
    }

    QString delegate()
    {
        return _delegate;
    }

    QString position()
    {
        return _position;
    }

    QString extProperty()
    {
        return _extProperty;
    }

    QObject* customDelegate()
    {
        return _customDelegate;
    }

protected:
    QString _frame;
    QUuid _uuid;
    QString _delegate;
    QString _position;
    QString _extProperty;
    QObject* _customDelegate;
};

}
