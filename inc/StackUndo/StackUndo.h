#ifndef STACKUNDO_H
#define STACKUNDO_H

#include <FoundationGlobal.h>
#include "AnalyzerCommand.h"
#include "StackUndo/AnalyzerWriteStream.h"
#include "StackUndo/StackObject.h"
#include <Units.h>
#include <QObject>
#include <QMetaType>
#include <QXmlStreamWriter>

namespace Planar {

class AnalyzerItem;
class AnalyzerWriteStream;

class FOUNDATIONSHARED_EXPORT StackUndo : public QObject
{
    Q_OBJECT
public:
    explicit StackUndo(QObject* parent = nullptr);

    void pushObjectToStack(const StackObject& object);
    void deleteObjectFromStack(const StackObject& object);
    void handleCommand(const AnalyzerCommand& command, const CommandStatus status,
                       AnalyzerWriteStream* streamWriter = nullptr);

    void doStepBack();
    void doStepForward();

    void objectAddProperty(AnalyzerItem* item)
    {
        _stackObjectsVector[_index].addItem(item);
    }

    void revertDependantChanges();

public slots:

private:
    void decrementIndex();
    void incrementIndex();
    bool isIndexLast() const;
    bool setObjectStatus(StackObject* objectPointer, const CommandStatus status);
    StackObject* findObject(const AnalyzerCommand& command);
    bool isCommandInStack(const AnalyzerCommand& command);
    void performLastCommand();
    void performCommand(StackObject* object, const AnalyzerCommand& command);
    int currentIndex() const;
    void packObject(StackObject* currentObject, const AnalyzerCommand& command);

    int _index;                     //индекс текущего объекта на выполнение
    QVector<StackObject> _stackObjectsVector;
};

//-----------------------------------------------------------------

} //namespace Planar
#endif // STACKUNDO_H
