#ifndef USERUNDOSTACK_H
#define USERUNDOSTACK_H

#include "AnalyzerCommand.h"
#include "StackUndo/UserUndoObject.h"
#include <QObject>
#include <QMutex>

namespace Planar {

//класс стэка Undo для представления пользователю
class FOUNDATIONSHARED_EXPORT UserUndoStack : public QObject
{
    Q_OBJECT
public:
    UserUndoStack(QObject* parent = nullptr) : QObject(parent), _index(-1) {}

    StackUndoPushResult appendToStack(const AnalyzerCommand& newCommand,
                                      AnalyzerWriteStream* streamWriter);
    void setCommandStatus(const AnalyzerCommand& command, const CommandStatus status);

    CommandStatus commandStatus(int index) const;
    const AnalyzerCommand& command(int index) const;
    AnalyzerWriteStream* streamWriter(const AnalyzerCommand& command);

    CommandStatus lastCommandStatus() const;
    int commandCount() const;
    int currentIndex() const;

    void decrementIndex();
    void incrementIndex();

    bool isIndexLast() const;

    //объединение множественных команд в одну путем замены величины
    void packCommand(const AnalyzerCommand& command);

private:
    UserUndoObject* findObjectByNumber(const AnalyzerCommand& command);
    inline void pushToCommandVector(const AnalyzerCommand& newCommand,
                                    AnalyzerWriteStream* streamWriter);
    UserUndoObject* findMultiObject(const AnalyzerCommand& command);
    inline UserUndoObject* findUserUndoObject(const AnalyzerCommand& command);

    QVector<UserUndoObject> _commandVector;

    //вектор описания команды для исп-я в интерфейсе
    //std::vector<QString> _commandNames;
    int _index;
    QMutex _mutex;
};

} //namespace Planar

#endif // USERUNDOSTACK_H
