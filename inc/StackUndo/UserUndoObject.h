#ifndef USERUNDOOBJECT_H
#define USERUNDOOBJECT_H

#include "AnalyzerCommand.h"
#include "StackUndo/AnalyzerWriteStream.h"

namespace Planar {

class FOUNDATIONSHARED_EXPORT UserUndoObject
{
public:
    UserUndoObject() : _packedCommandNumber(0) {}
    UserUndoObject(const AnalyzerCommand& command, AnalyzerWriteStream* streamWriter);
    UserUndoObject(const UserUndoObject& object);

    const AnalyzerCommand& command() const
    {
        return _command;
    }

    AnalyzerCommand* commandPtr()
    {
        return &_command;
    }

    AnalyzerWriteStream* streamWriterPtr() const
    {
        Q_ASSERT(this != nullptr);
        if (this == nullptr)
            return nullptr;
        return _streamWritePtr;
    }

    long packedCommandNumber() const
    {
        return _packedCommandNumber;
    }

    void setPackedCommandNumber(const long number)
    {
        _packedCommandNumber = number;
    }

private:
    AnalyzerCommand _command;
    AnalyzerWriteStream* _streamWritePtr;
    long _packedCommandNumber;   //номер последней упакованной команды, если она была множественной
};

} //namespace Planar

#endif // USERUNDOOBJECT_H
