#ifndef STACKOBJECT_H
#define STACKOBJECT_H

#include <FoundationGlobal.h>
#include "AnalyzerCommand.h"
#include "StackUndo/AnalyzerWriteStream.h"
#include <Units.h>
#include <QQueue>
#include <QXmlStreamWriter>
#include <QObject>
#include <QMetaType>
#include <QDateTime>
#include <QFile>
#include <QIODevice>

namespace Planar {

class AnalyzerItem;
class AnalyzerWriteStream;

struct DependantStackObject {
    DependantStackObject() {}
    DependantStackObject(AnalyzerPropertyBase* ptr, const int id, const QVariant val):
        _handlerHolderPtr(ptr), _id(id), _value(val) {}
    DependantStackObject(const DependantStackObject& source)
    {
        _handlerHolderPtr = source._handlerHolderPtr;
        _id = source._id;
        _value = source._value;
    }

    //указатель на владельца метода-обработчика
    AnalyzerPropertyBase* _handlerHolderPtr;

    //индекс мета-метода, обработчика
    int _id;
    QVariant _value;
};

class FOUNDATIONSHARED_EXPORT StackObject
{

public:
    StackObject() : _lastPackedCommandNumber(0), _isReverted(false) {}
    StackObject(const AnalyzerCommand& command, AnalyzerWriteStream* stream);
    StackObject(const StackObject& obj);
    ~StackObject();

    void doCommand(const bool write = true);
    void undoCommand();

    AnalyzerCommand* command()
    {
        return &_commandDo;
    }

    const AnalyzerCommand* command() const
    {
        return &_commandDo;
    }

    void addItem(AnalyzerItem* item)
    {
        _item = item;
    }

    bool operator==(const StackObject& rhs)
    {
        return (_commandDo.commandNumber() == const_cast<StackObject&>(rhs).command()->commandNumber());
    }

    bool operator==(const StackObject& rhs) const
    {
        return (_commandDo.commandNumber() == rhs.command()->commandNumber());
    }

    AnalyzerItem* item() const
    {
        return _item;
    }

    QString filename() const
    {
        return _fileName;
    }

    AnalyzerWriteStream* stream() const
    {
        return _streamWriter;
    }

    /* QDateTime time() const
     {
         return _time;
     }*/

    void setLastPackedCommandNumber(const long number)
    {
        _lastPackedCommandNumber = number;
    }

    long lastPackedCommandNumber() const
    {
        return _lastPackedCommandNumber;
    }

    bool dependantQueueIsEmpty() const;
    DependantStackObject dependantQueueObject();

    bool isReverted() const
    {
        return _isReverted;
    }
    void setNotReverted()
    {
        _isReverted = false;
    }
    void setReverted()
    {
        _isReverted = true;
    }

private:
    QVariant getConditionBefore(QXmlStreamReader& xmlReader);
    inline void invokeHandlerMethod();

    AnalyzerCommand _commandDo;
    // AnalyzerCommand* _commandUndo;

    AnalyzerItem* _item;

    QString _fileName;               //имя файла для хранения старых значений свойств
    AnalyzerWriteStream* _streamWriter;

    //номер последней упакованной множественной команды в этом объекте
    long _lastPackedCommandNumber;

    //QDateTime _time;

    //очередь зависимых изменений. Изменения должны возвращаться в том же порядке, в каком они производились
    //иначе следующая измененная величина не сможет вернуть прошлое значение из-за несоответствия предыдущей
    QQueue<DependantStackObject> _dependantChangesQueue;
    bool _isReverted;
};

} //namespace Planar
#endif // STACKOBJECT_H
