#ifndef ANALYZERWRITESTREAM_H
#define ANALYZERWRITESTREAM_H

#include <QXmlStreamWriter>

namespace Planar {

//TODO: переименовать во что-то более общее, не аналайзер
class AnalyzerWriteStream
{
public:
    explicit AnalyzerWriteStream() : _isOpen(false) {}

    QXmlStreamWriter* streamPtr()
    {
        return &_stream;
    }

    void openStream()
    {
        _isOpen = true;
    }

    void closeStream()
    {
        _isOpen = false;
    }

    bool isOpen() const
    {
        return _isOpen;
    }

private:
    QXmlStreamWriter _stream;
    bool _isOpen;
};

}

#endif // ANALYZERWRITESTREAM_H
