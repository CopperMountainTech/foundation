#ifndef STACKUNDODISPATCHER_H
#define STACKUNDODISPATCHER_H

#include "StackUndo/StackUndo.h"
#include "AnalyzerCommand.h"
#include "StackUndo/UserUndoStack.h"
#include <QObject>
#include <QVector>

namespace Planar {

class AnalyzerItem;

class FOUNDATIONSHARED_EXPORT StackUndoDispatcher : public QObject
{
    Q_OBJECT
public:
    StackUndoDispatcher(AnalyzerItem* parent = nullptr);

    //Добавление команды в стэк (вызывается перед добавлением команды в очередь Flux)
    StackUndoPushResult appendToUserStack(const AnalyzerCommand& command,
                                          AnalyzerWriteStream* streamWriter);
    //Изменение статуса команды внутри стэка.
    //Running - команда начинает выполняться
    //Completed - команда выполнена
    void setCommandStatus(const AnalyzerCommand& command, const CommandStatus status);

    void undoLastCommand();                //Откат одной команды
    void undoCommandToIndex(int toIndex);  //Откат всех команд до индекса
    void redoLastCommand();                //Повтор одной команды
    void redoCommandIndex(int toIndex);    //Повтор всех команд до индекса

    //Значение счетчика откатов (для индикации числа невыполненных откатов)
    int undoWaitCounter() const
    {
        return _undoWaitCounter;
    }
signals:
    void lastElementReached();
    void firstElementReached();
    void waitUndoCounterIsNotZero();

public slots:

private:
    void undoCommandMake(const int index);
    void redoCommandMake(const int index);
    int commandStackSize() const;
    int currentUserStackIndex() const;

    //стэк изменений для пользователя. Хранит команды и их статусы
    //Используется для undo и redo незавершенных команд (InQueue)
    UserUndoStack _userUndoStack;

    //стэк с данными о прошлом изменении (хранятся в файлах). Хранит только завершенные команды (Completed)
    //Используется для undo и redo завершенных команд
    StackUndo _analyzerUndoStack;

    //указатель на корневой класс иерархии VnaAnalyzer. Нужен для поиска команд в очередях его и потомков
    AnalyzerItem* _analyzerPtr;

    int _undoWaitCounter;             //счетчик откатов, которые нужно будет выполнить после окончания выполнения команды, которая сейчас Running
    bool _undoAllDependantValuesBack; //флаг показывает, что нужно вернуть обратно величины всех доп. свойств, затронутые командой (зависимое изменение)
};

void deleteCommandFromQueue(AnalyzerItem* object, const AnalyzerCommand& command);

} //namespace Planar


#endif // STACKUNDODISPATCHER_H
