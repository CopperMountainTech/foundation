#pragma once

#include <QUuid>

namespace Planar {

constexpr QUuid CreateUuid(const quint32 part32, const quint16 part16_1, const quint16 part16_2,
                           const quint16 part16_3, const quint64 part64)
{
    return QUuid(part32, part16_1, part16_2,
                 (part16_3 & 0xff00) >> 8,
                 part16_3 & 0xff,
                 (part64 & 0xff0000000000) >> 40,
                 (part64 & 0xff00000000) >> 32,
                 (part64 & 0xff000000) >> 24,
                 (part64 & 0xff0000) >> 16,
                 (part64 & 0xff00) >> 8,
                 part64 & 0xff);
}

} //namespace Planar
