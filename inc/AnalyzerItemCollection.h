#pragma once

#include <QList>

#include "AnalyzerItem.h"


namespace Planar {

class FOUNDATIONSHARED_EXPORT AnalyzerItemCollectionBase : public AnalyzerItem
{
    Q_OBJECT
    Q_PROPERTY(int size READ size NOTIFY sizeChanged)
    Q_PROPERTY(IntProperty* activeItemIndex READ activeItemIndex CONSTANT)

public:
    AnalyzerItemCollectionBase(AnalyzerItem* parent, int minSize);

    ~AnalyzerItemCollectionBase() {}

    // число элементов коллекции
    virtual int size() const = 0;

    // индекс item в коллекции. -1, если item не принадлежит коллекции
    virtual int indexOf(AnalyzerItem* item) const = 0;

    // приведенный к AnalyzerItem* указатель на элемент коллекции по индексу
    virtual AnalyzerItem* baseItemAt(int index) const = 0;

    // устанавливает item активным в коллекции
    // ничего, если item не принадлежит коллекции
    virtual void setActiveBaseItem(AnalyzerItem* item) = 0; // protected??

    // приведенный к AnalyzerItem* указатель на активный элемент коллекции
    virtual AnalyzerItem* activeBaseItem() const = 0;

    // свойство, определяющее индекс активного элемента в коллекции
    IntProperty* activeItemIndex()
    {
        return &_activeItemIndex;
    }

    // запрос на удаление элемента коллекции по индексу
    // если число элементов меньше _minimalSize, запрос не формируется
    Q_INVOKABLE void requestRemoveItem(int index);

    // запрос на удаление активного элемента
    // если число элементов меньше _minimalSize, запрос не формируется
    Q_INVOKABLE void requestRemoveActiveItem();

    // запрос на добавление элемента в коллекцию
    void requestAddItem(QVariantList paramList);

signals:
    void sizeChanged(int size);

    void activeItemChanged();
    int activeItemIndexChanged(int index);

    void beforeItemAdded();
    void itemAdded(AnalyzerItem* item);

    void beforeItemRemoved(AnalyzerItem* item);
    void itemRemoved();

public slots:
    void onAddItemCommand(QVariant params);
    virtual void onRemoveItemCommand(QVariant value);
    //void onPreActiveItemIndexChanging(int& result, bool& isApproved);
    //void onActiveItemIndexChanged(int value);

protected:
    virtual void resetItemActiveStates() = 0;

    virtual void removeAt(int index) = 0;

    void removeActiveItem();

    virtual void addChild(QVariantList params)
    {
        Q_UNUSED(params);
    }

    IntProperty _activeItemIndex;

    int _minimalSize;

    //Объекты-команды на изменение конфигурации
    CommandProperty _requestRemoveItem;
    CommandProperty _requestAddChild;
};


template <class T>
class AnalyzerItemCollection : public AnalyzerItemCollectionBase
{
public:
    AnalyzerItemCollection(AnalyzerItem* parent, int minSize)
        : AnalyzerItemCollectionBase(parent, minSize)
    { }

    ~AnalyzerItemCollection()
    {
        //clear();
    }

    //--------------------- overrides ----------------------------------

    int size() const override
    {
        return _collection.size();
    }


    int indexOf(AnalyzerItem* item) const override
    {
        return _collection.indexOf(qobject_cast<T*>(item));
    }

    AnalyzerItem* baseItemAt(int index) const override
    {
        return at(index);
    }

    void setActiveBaseItem(AnalyzerItem* item)
    {
        setActiveItem(qobject_cast<T*>(item));
    }

    AnalyzerItem* activeBaseItem() const override
    {
        return activeItem();
    }

    //-------------------------------------------------------

    // элемент коллекции по индексу
    T* at(int index) const
    {
        Q_ASSERT(index < size());
        return _collection.at(index);
    }

    // добавляет элемент в коллекцию и делает активным
    void add(T* item)
    {
        emit beforeItemAdded();

        _collection.append(item);
        item->setCollection(this);
        resetItemCollectionIndices();
        // после _collection.append(item), перед сигналами

        _parentItem->addChild(item);

        emit sizeChanged(_collection.size());

        _activeItemIndex.acceptMax(size() - 1);
        _activeItemIndex.acceptValue(size() - 1, true);  // see Note 1
        resetItemActiveStates();

        emit itemAdded(item);
    }

    // удаляет указатель и объект по индексу
    // перед удалением другой элемент становится активным
    void removeAt(int index) override
    {
        if (size() <= _minimalSize || index < 0 || index >= size())
            return;

        int activeId = _activeItemIndex.value();
        if (activeId == -1) return;

        // сначала сделаем активным другой элемент - для SWAN
        if (activeId == index) {
            if (index > 0) {
                _activeItemIndex.acceptValue(index - 1);
            } else if (index == 0 && _collection.size() > 1) {
                _activeItemIndex.acceptValue(index + 1);
            } else {
                _activeItemIndex.acceptValue(-1);
            }
        }

        auto lastActiveItem = activeItem();
        auto item = _collection.at(index);

        emit beforeItemRemoved(item);

        _collection.removeOne(item);
        setActiveItem(lastActiveItem);
        _activeItemIndex.acceptMax(size() - 1);

        _parentItem->removeChild(item);   // destructor
        resetItemCollectionIndices();    // после вызова removeChild(item)

        emit sizeChanged(_collection.size());
        emit itemRemoved();
    }

    // удаляет указатель и объект
    void remove(T* item)
    {
        Q_ASSERT(nullptr != item);
        int index = _collection.indexOf(item);
        if (index != -1) removeAt(index);
    }

    // true, если объект уже есть в коллекции
    bool exists(T* item) const
    {
        return _collection.indexOf(item) != -1;
    }

    // удаляет все объекты и все указатели из коллекции
    void clear()
    {
        for (T* item : _collection) {
            _parentItem->removeChild(item);
        }
        _collection.clear();
        _activeItemIndex.acceptValue(-1, true); // see Note 1
        _activeItemIndex.acceptMax(0);
    }

    // указатель на активный элемент коллекции
    // nullptr, если коллекция пуста или нет активного элемента
    T* activeItem() const
    {
        int index = _activeItemIndex.value();
        return index == -1 ? nullptr : _collection.at(index);
    }

    // устанавливает элемент активным в коллекции
    // ничего, если элемент не принадлежит коллекции
    void setActiveItem(T* item)
    {
        int index = _collection.indexOf(item);
        if (index != -1) {
            _activeItemIndex.acceptValue(index, true); // true - see Note 1
        }
    }

    // ссылка на коллекцию элементов для использования с for, QtAlgorithms etc
    QList<T*>& items()
    {
        return _collection;
    }

protected:
    void resetItemActiveStates() override
    {
        for (int i = 0; i < _collection.size(); ++i) {
            _collection.at(i)->resetActiveState(i == _activeItemIndex.value());
        }
    }

    void resetItemCollectionIndices()
    {
        for (int i = 0; i < _collection.size(); ++i) {
            _collection.at(i)->resetCollectionIndex(i);
        }
    }

    QList<T*> _collection;
};

// Note 1. не всегда активный объект меняется вместе с индексом
// при удалении индекс может остаться прежним, а объект смениться
// т.е. нельзя менять активный элемент по сигналу смены индекса

} // namespace Planar



