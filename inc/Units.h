#pragma once

#include "FoundationGlobal.h"
#include <QString>
#include <QObject>
#include <MathEssentials.h>

namespace Planar {

struct FrequencyUnit;
struct LogRatioUnit;
struct PowerUnit;

enum class UnitType {
    Plain,
    Frequency,
    LogRatio,
    Power,
    MetricLength,
    ImperialLength,
    Time,
    Phase,
    Resistance,
    Conductance,
    Capacitance,
    Inductance,
    SampleRate,
};


struct PartialUnit {
    double multiplier;
    const QString symbol;
};


struct UnitSpecification {
    const UnitType type;
    const PartialUnit& SI;
    const PartialUnit& Vna;
    const PartialUnit**  normTable;
    const int normTableSize;
    const bool e10SI;
};


//------------------------ Unit -------------------------------------


class FOUNDATIONSHARED_EXPORT Unit
{


public:
    Unit(const Unit& prototype) : _value(prototype._value),
        _multiplier(prototype._multiplier), _specification(prototype._specification) {}

    // создает объект со спецификацией и множителем прототипа, но с произвольным значением value
    Unit(double value, const Unit& prototype) : _value(value), _multiplier(prototype._multiplier),
        _specification(prototype._specification) {}

    // для контролов
    Unit();

    static constexpr double Pico  = 1E-12;
    static constexpr double Nano  = 1E-9;
    static constexpr double Micro = 1E-6;
    static constexpr double Milli = 1E-3;
    static constexpr double Centi = 0.01;
    static constexpr double Kilo  = 1E3;
    static constexpr double Mega  = 1E6;
    static constexpr double Giga  = 1E9;
    static constexpr double Tera  = 1E12;

    static constexpr double Meter2Inch = 0.0254;
    static constexpr double Meter2Feet = 12 * Meter2Inch;
    static constexpr double Meter2Mile = 5280 * Meter2Feet;
    static constexpr double Radian2Degree =  180.0 / M_PI;
    static constexpr double Degree2Radian =  M_PI / 180.0;

    static constexpr double UnitEpsilon = 10E-14;
    static constexpr double MinEpsilon = 10E-80;


    const UnitSpecification& specification() const
    {
        return *_specification;
    }

    // значение величины
    double value() const
    {
        return _value;
    }

    /*!
     * значение величины при множителе multiplier
     * auto f = Megahertz(5); f.valueIn(Kilo) вернет 5000
     */
    double toDouble(double multiplier) const
    {
        return convertValue(_value, _multiplier, multiplier);
    }

    // множитель
    double multiplier() const
    {
        return _multiplier;
    }

    /*!
     * целая степень десятки множителя: multiplier() вернет 100, multiplierE10() вернет 2
     * имеет смысл только для тех величин, у которых множитель - степень десятки, _specification->E10SI == true
     * не имеет смысла для фазы и неметрических длин
     */
    int multiplierE10() const;

    // значение величины в рабочих единицах. auto f = Megahertz(10); f.ValueNA() вернет 10
    double valueVna() const;

    // значение величины в СИ. auto f = Megahertz(10); f.valueSI() вернет 10000000
    double toDouble() const
    {
        return _value * _multiplier;    // ??? / _specification->SI.multiplier;
    }

    // приводит к наиболее удобному для представления множителю. набор множителей задан в UnitSpecification
    Unit& normalize();

    // преобразует величину к единице СИ данного типа измерения (в Гц для частоты)
    Unit& toSI()
    {
        changeMultiplier(_specification->SI.multiplier);
        return *this;
    }

    // преобразует величину к рабочей единице анализатора для данного типа измерения (в МГц для частоты)
    Unit& toVna()
    {
        changeMultiplier(_specification->Vna.multiplier);
        return *this;
    }

    // устанавливает множитель
    Unit& toPico()
    {
        changeMultiplier(Pico);
        return *this;
    }
    Unit& toNano()
    {
        changeMultiplier(Nano);
        return *this;
    }
    Unit& toMicro()
    {
        changeMultiplier(Micro);
        return *this;
    }
    Unit& toMilli()
    {
        changeMultiplier(Milli);
        return *this;
    }
    Unit& toCenti()
    {
        changeMultiplier(Centi);
        return *this;
    }
    Unit& toKilo()
    {
        changeMultiplier(Kilo);
        return *this;
    }
    Unit& toMega()
    {
        changeMultiplier(Mega);
        return *this;
    }
    Unit& toGiga()
    {
        changeMultiplier(Giga);
        return *this;
    }
    Unit& toTera()
    {
        changeMultiplier(Tera);
        return *this;
    }

    // рабочая единица анализатора для данного типа измерения (в MHz для частоты)
    const PartialUnit& unitVna()
    {
        return _specification->Vna;
    }

    // единица СИ данного типа измерения
    const PartialUnit& unitSI()
    {
        return _specification->SI;
    }

    // изменяет _multiplier и изменяет _value, так чтобы произведение _value * _multiplier не изменилось
    Unit& changeMultiplier(double multiplier)
    {
        _value = _value * _multiplier / multiplier;
        _multiplier = multiplier;
        return *this;
    }

    // устанавливает _multiplier таким же, как у unit
    Unit& changeMultiplierAs(const Unit& unit)
    {
        changeMultiplier(unit.multiplier());
        return *this;
    }

    // устанавливает Value в единице измерения данной величины
    Unit& setValue(double value)
    {
        _value = value;
        return *this;
    }

    // устанавливает Value в единице измерения данной величины, при этом параметр siValue - значение в СИ
    // auto f = Megahertz(10); f.SetValueFromSI(1000000); Vаlue установится в 1
    Unit& setValueFromSI(double siValue)
    {
        _value = siValue / _multiplier;
        return *this;
    }

    // устанавливает Value в единице измерения данной величины, при этом параметр naValue - значение в рабочих единицах
    // auto f = Megahertz(10); f.SetValueFromNA(30); Vаlue установится в 30
    Unit& setValueFromVna(double vnaValue)
    {
        _value = vnaValue * _specification->Vna.multiplier / _multiplier;
        return *this;
    }

    // преобразует значение из СИ в единицу данной величины. f = Megahertz(10). f.valueSI2This(1000000) вернет 1
    // при этом f.Value по барабану, используеся f.Unit. f = Megahertz(300). f.valueSI2This(1000000) тоже вернет 1
    double convertValueSIToThis(double value) const
    {
        return convertValue(value, _specification->SI.multiplier, _multiplier);
    }

    // преобразует значение из единицы данной величины в СИ. f = Megahertz(10). f.ValueThis2SI(1) вернет 1000000
    // при этом f.Value по барабану, используеся f.Unit. f = Megahertz(300). f.ValueThis2SI(1) тоже вернет 1000000
    double convertValueThisToSI(double value)  const
    {
        return convertValue(value, _multiplier, _specification->SI.multiplier);
    }

    // преобразует значение из СИ в рабочую единицу анализатора. подробнее см. valueSI2This()
    double convertValueSIToVna(double value)  const
    {
        return convertValue(value, _specification->SI.multiplier, _specification->Vna.multiplier);
    }

    // преобразует значение из рабочей единицы анализатора в СИ. подробнее см. valueSI2This()
    double convertValueVnaToSI(double value)  const
    {
        return convertValue(value, _specification->Vna.multiplier, _specification->SI.multiplier);
    }

    // преобразует значение из рабочей единицы анализатора в единицу данной величины. подробнее см. valueSI2This()
    double convertValueVnaToThis(double value)  const
    {
        return convertValue(value, _specification->Vna.multiplier, _multiplier);
    }

    // преобразует значение из единицы данной величины в  рабочую единицу анализатора. подробнее см. valueSI2This()
    double convertValueThisToVna(double value)  const
    {
        return convertValue(value, _multiplier, _specification->Vna.multiplier);
    }

    // символьное обозначение текущей единицы (единицы, в которой представлено значение Value())
    const QString& symbol() const;

    // строковое представление, форматирование 'g'. withSymbol == true добавляет Symbol() ("10 МНz")
    QString toString(bool withSymbol = true) const;

    // строковое представление, precision - число знаков после запятой. withSymbol == true добавляет Symbol()
    QString toString(int precision, bool withSymbol = true) const;

    // строковое представление, decimals - общее число десятичных знаков. withSymbol == true добавляет Symbol()
    QString toStringNDecimals(int decimals, bool withSymbol = true) const;

    // примерное равенство измеряемых величин в единицах СИ с погрешностью epsilon
    bool isApproximateEqualSI(const Unit& unit) const
    {
        return isSameType(unit) && isEqual(toDouble(), unit.toDouble());
    }

    // примерное равенство измеряемых величин в единицах NA с погрешностью epsilon
    bool isApproximateEqualVna(const Unit& unit) const
    {
        return isSameType(unit) && isEqual(valueVna(), unit.valueVna());
    }

    // примерное равенство измеряемой величины в единицах СИ параметру value с погрешностью epsilon
    bool isApproximateEqualSI(double value) const
    {
        return isEqual(toDouble(), value);
    }

    // примерное равенство измеряемой величины в единицах NA параметру value с погрешностью epsilon
    bool isApproximateEqualVna(double value) const
    {
        return isEqual(valueVna(), value);
    }

    // примерное равенство измеряемой величины в единицах СИ нулю с погрешностью epsilon
    bool isApproximateZeroSI() const
    {
        return isZero(toDouble());
    }

    // примерное равенство измеряемой величины в единицах NA нулю с погрешностью epsilon
    bool isApproximateZeroVna() const
    {
        return isZero(valueVna());
    }

    // true, если одинаковый тип измеряемой величины
    bool isSameType(const Unit& unit) const
    {
        return _specification->type == unit.specification().type;
    }

    bool isFrequency() const
    {
        return _specification->type == UnitType::Frequency;
    }
    bool isPower() const
    {
        return _specification->type == UnitType::Power;
    }
    bool isLogRatio() const
    {
        return _specification->type == UnitType::LogRatio;
    }
    bool isLength() const
    {
        return _specification->type == UnitType::MetricLength
               || _specification->type == UnitType::ImperialLength;
    }
    bool isTime() const
    {
        return _specification->type == UnitType::Time;
    }
    bool isPhase() const
    {
        return _specification->type == UnitType::Phase;
    }
    bool isResistance() const
    {
        return _specification->type == UnitType::Resistance;
    }
    bool isConductance() const
    {
        return _specification->type == UnitType::Conductance;
    }
    bool isCapacitance() const
    {
        return _specification->type == UnitType::Capacitance;
    }
    bool isInductance() const
    {
        return _specification->type == UnitType::Inductance;
    }

    int normalizeTableIndex() const;

    //-----------  operators ------------------------------------------------------

    Unit& operator = (const Unit& unit)
    {
        if (this != &unit) {
            _value         = unit._value;
            _multiplier    = unit._multiplier;
            _specification = unit._specification;
        }
        return *this;
    }

    // точнное равенство, предполагает задание одинаковыми константами ???
    // bool operator == (const Unit& unit) const { return sameType(unit) && valueSI() == unit.valueSI(); }
    // bool operator != (const Unit& unit) const { return !(*this == unit); }

    bool operator < (const Unit& unit) const
    {
        Q_ASSERT(isSameType(unit));
        return toDouble() < unit.toDouble();
    }

    bool operator <= (const Unit& unit) const
    {
        Q_ASSERT(isSameType(unit));
        return toDouble() <= unit.toDouble();
    }

    bool operator > (const Unit& unit) const
    {
        Q_ASSERT(isSameType(unit));
        return toDouble() > unit.toDouble();
    }

    bool operator >= (const Unit& unit) const
    {
        Q_ASSERT(isSameType(unit));
        return toDouble() >= unit.toDouble();
    }

    Unit& operator += (const Unit& unit)
    {
        Q_ASSERT(isSameType(unit));
        _value += convertValue(unit.value(), unit.multiplier(), _multiplier);
        return *this;
    }

    Unit& operator -= (const Unit& unit)
    {
        Q_ASSERT(isSameType(unit));
        _value -= convertValue(unit.value(), unit.multiplier(), _multiplier);
        return *this;
    }

    Unit operator + (const Unit& unit) const
    {
        Q_ASSERT(isSameType(unit));
        return Unit(_value + convertValue(unit.value(), unit.multiplier(), _multiplier), *this);
    }

    Unit operator - (const Unit& unit) const
    {
        Q_ASSERT(isSameType(unit));
        return Unit(_value - convertValue(unit.value(), unit.multiplier(), _multiplier), *this);
    }

    Unit& operator += (double value)
    {
        _value += value;
        return *this;
    }

    Unit& operator -= (double value)
    {
        _value -= value;
        return *this;
    }

    Unit& operator *= (double value)
    {
        _value *= value;
        return *this;
    }

    Unit& operator /= (double value)
    {
        Q_ASSERT(!isZero(value));
        _value /= value;
        return *this;
    }

    Unit operator + (double value) const
    {
        return Unit(_value + value, *this);
    }
    Unit operator - (double value) const
    {
        return Unit(_value - value, *this);
    }
    Unit operator * (double value) const
    {
        return Unit(_value * value, *this);
    }
    Unit operator / (double value) const
    {
        Q_ASSERT(!isZero(value));
        return Unit(_value / value, *this);
    }

    //const UnitSpecification& Specification;

protected:
    double _value;
    double _multiplier;

    const UnitSpecification* _specification;

    Unit(const UnitSpecification& specification, double value, double multiplier) :
        _value(value), _multiplier(multiplier), _specification(&specification) {}

//    Unit(const Unit& source, const PartialUnit& unit) : Unit(unit)
//    {
//        Value = source.Value * source._multiplier / unit.multiplier;
//    }

    void appendSuffix(QString& string, bool withUnit) const;

    static double convertValue(double value, double srcmultiplier, double destmultiplier)
    {
        return value * srcmultiplier / destmultiplier;
    }
};


//------------------------- PlainValue ------------------------------------


struct FOUNDATIONSHARED_EXPORT PlainValue : public Unit {
    PlainValue(double value = 0);
};

//----------------- Decibel --------------------------------------


struct FOUNDATIONSHARED_EXPORT Decibel : public Unit {
    Decibel(double value = 0);
};


struct FOUNDATIONSHARED_EXPORT DbmW : public Unit {
    DbmW(double value = 0);
};


//----------------------------- FrequencyUnit -----------------------------------------------------


struct FOUNDATIONSHARED_EXPORT FrequencyUnit : public Unit {
protected:
    FrequencyUnit(double value, double multiplier);
};


struct FOUNDATIONSHARED_EXPORT Hertz : public FrequencyUnit {
    Hertz(double value = 1) : FrequencyUnit(value, 1) {}
};


struct FOUNDATIONSHARED_EXPORT HertzE10 : public FrequencyUnit {
    HertzE10(double value, int e10);
};


struct Kilohertz : public FrequencyUnit {
    Kilohertz(double value = 1) : FrequencyUnit(value, Kilo) {}
};


struct Megahertz : public FrequencyUnit {
    Megahertz(double value = 1) : FrequencyUnit(value, Mega) {}
};


struct Gigahertz : public FrequencyUnit {
    Gigahertz(double value = 1) : FrequencyUnit(value, Giga) {}
};


//---------------------------------- Power -------------------------------------------------------


struct FOUNDATIONSHARED_EXPORT PowerUnit : public Unit {
protected:
    PowerUnit(double value, double multiplier);
};


struct FOUNDATIONSHARED_EXPORT Watt : public PowerUnit {
    Watt(double value = 1) : PowerUnit(value, 1) {}
};

struct FOUNDATIONSHARED_EXPORT Milliwatt : public PowerUnit {
    Milliwatt(double value = 1) : PowerUnit(value, Milli) {}
};

struct FOUNDATIONSHARED_EXPORT Kilowatt : public PowerUnit {
    Kilowatt(double value = 1) : PowerUnit(value, Kilo) {}
};

struct FOUNDATIONSHARED_EXPORT Megawatt : public PowerUnit {
    Megawatt(double value = 1) : PowerUnit(value, Mega) {}
};


//--------------------- Time ----------------------------------------------


struct FOUNDATIONSHARED_EXPORT TimeUnit : public
    Unit {   // NA unit is ns for MeasurementTrace::Argument2Unit
protected:
    TimeUnit(double value, double multiplier);
};


struct FOUNDATIONSHARED_EXPORT Picosecond : public TimeUnit {
    Picosecond(double value = 1) : TimeUnit(value, Pico) {}
};

struct FOUNDATIONSHARED_EXPORT Nanosecond : public TimeUnit {
    Nanosecond(double value = 1) : TimeUnit(value, Nano) {}
};

struct FOUNDATIONSHARED_EXPORT Microsecond : public TimeUnit {
    Microsecond(double value = 1) : TimeUnit(value, Micro) {}
};

struct FOUNDATIONSHARED_EXPORT Millisecond : public TimeUnit {
    Millisecond(double value = 1) : TimeUnit(value, Milli) {}
};

struct FOUNDATIONSHARED_EXPORT Second : public TimeUnit {
    Second(double value = 1) : TimeUnit(value, 1) {}
};


//-------------------------------- LengthUnit ------------------------------------------


struct FOUNDATIONSHARED_EXPORT LengthUnit : public Unit {
protected:
    LengthUnit(double value, double multiplier);
};


struct FOUNDATIONSHARED_EXPORT Meter : public LengthUnit {
    Meter(double value = 1) : LengthUnit(value, 1) {}
};

struct FOUNDATIONSHARED_EXPORT Millimeter : public LengthUnit {
    Millimeter(double value = 1) : LengthUnit(value, Milli) {}
};

struct FOUNDATIONSHARED_EXPORT Centimeter : public LengthUnit {
    Centimeter(double value = 1) : LengthUnit(value, Centi) {}
};

struct FOUNDATIONSHARED_EXPORT Kilometer : public LengthUnit {
    Kilometer(double value = 1) : LengthUnit(value, Kilo) {}
};


//------------------------ ImperialLength -----------------------------------------


struct FOUNDATIONSHARED_EXPORT ImperialLengthUnit : public Unit {
protected:
    ImperialLengthUnit(double value, double multiplier);
};


struct FOUNDATIONSHARED_EXPORT Inch: public ImperialLengthUnit {
    Inch(double value = 1) : ImperialLengthUnit(value, Meter2Inch) {}
};

struct FOUNDATIONSHARED_EXPORT Feet : public ImperialLengthUnit {
    Feet(double value = 1) : ImperialLengthUnit(value, Meter2Feet) {}
};

struct FOUNDATIONSHARED_EXPORT Mile : public ImperialLengthUnit {
    Mile(double value = 1) : ImperialLengthUnit(value, Meter2Mile) {}
};


//------------------ Phase -----------------------------------------------


struct FOUNDATIONSHARED_EXPORT PhaseUnit : public Unit {
    Unit& toRadian()
    {
        changeMultiplier(1);
        return *this;
    }
    Unit& toDegree()
    {
        changeMultiplier(Degree2Radian);
        return *this;
    }

protected:
    PhaseUnit(double value, double multiplier);
};


struct FOUNDATIONSHARED_EXPORT Radian : public PhaseUnit {
    Radian(double value = 0) : PhaseUnit(value, 1) {}
};


struct FOUNDATIONSHARED_EXPORT Degree : public PhaseUnit {
    Degree(double value = 0) : PhaseUnit(value, Degree2Radian) {}
};


//---------------------- Resistance -------------------------------------


struct FOUNDATIONSHARED_EXPORT ResistanceUnit : public Unit {
protected:
    ResistanceUnit(double value, double multiplier);
};


struct FOUNDATIONSHARED_EXPORT Ohm : public ResistanceUnit {
    Ohm(double value = 1) : ResistanceUnit(value, 1) {}
};

struct FOUNDATIONSHARED_EXPORT Kiloohm : public ResistanceUnit {
    Kiloohm(double value = 1) : ResistanceUnit(value, Kilo) {}
};

struct FOUNDATIONSHARED_EXPORT Megaohm : public ResistanceUnit {
    Megaohm(double value = 1) : ResistanceUnit(value, Mega) {}
};


//---------------------- ConductanceUnit -------------------------------------


struct FOUNDATIONSHARED_EXPORT ConductanceUnit : public Unit {
protected:
    ConductanceUnit(double value, double multiplier);
};


struct FOUNDATIONSHARED_EXPORT Siemens : public ConductanceUnit {
    Siemens(double value = 1) : ConductanceUnit(value, 1) {}

};

struct FOUNDATIONSHARED_EXPORT KiloSiemens : public ConductanceUnit {
    KiloSiemens(double value = 1) : ConductanceUnit(value, Kilo) {}
};

struct FOUNDATIONSHARED_EXPORT MilliSiemens : public ConductanceUnit {
    MilliSiemens(double value = 1) : ConductanceUnit(value, Milli) {}
};

//--------------------- CapacitanceUnit ----------------------------------------------


struct FOUNDATIONSHARED_EXPORT CapacitanceUnit : public Unit {
protected:
    CapacitanceUnit(double value, double multiplier);
};


struct FOUNDATIONSHARED_EXPORT Picofarad : public CapacitanceUnit {
    Picofarad(double value = 1) : CapacitanceUnit(value, Pico) {}
};

struct FOUNDATIONSHARED_EXPORT Nanofarad : public CapacitanceUnit {
    Nanofarad(double value = 1) : CapacitanceUnit(value, Nano) {}
};

struct FOUNDATIONSHARED_EXPORT Microfarad : public CapacitanceUnit {
    Microfarad(double value = 1) : CapacitanceUnit(value, Micro) {}
};

struct FOUNDATIONSHARED_EXPORT Millifarad : public CapacitanceUnit {
    Millifarad(double value = 1) : CapacitanceUnit(value, Milli) {}
};

struct FOUNDATIONSHARED_EXPORT Farad : public CapacitanceUnit {
    Farad(double value = 1) : CapacitanceUnit(value, 1) {}
};


//--------------------- InductanceUnit ----------------------------------------------


struct FOUNDATIONSHARED_EXPORT InductanceUnit : public Unit {
protected:
    InductanceUnit(double value, double multiplier);
};


struct FOUNDATIONSHARED_EXPORT Picohenry : public InductanceUnit {
    Picohenry(double value = 1) : InductanceUnit(value, Pico) {}
};

struct FOUNDATIONSHARED_EXPORT Nanohenry : public InductanceUnit {
    Nanohenry(double value = 1) : InductanceUnit(value, Nano) {}
};

struct FOUNDATIONSHARED_EXPORT Microhenry : public InductanceUnit {
    Microhenry(double value = 1) : InductanceUnit(value, Micro) {}
};

struct FOUNDATIONSHARED_EXPORT Millihenry : public InductanceUnit {
    Millihenry(double value = 1) : InductanceUnit(value, Milli) {}
};

struct FOUNDATIONSHARED_EXPORT Henry : public InductanceUnit {
    Henry(double value = 1) : InductanceUnit(value, 1) {}
};

//--------------------- SampleRate ------------------------------------------

struct FOUNDATIONSHARED_EXPORT SampleRateUnit : public Unit {
protected:
    SampleRateUnit(double value, double multiplier);
};

struct FOUNDATIONSHARED_EXPORT SamplesPerSecond : public SampleRateUnit {
    SamplesPerSecond(double value = 1) : SampleRateUnit(value, 1) {}
};

struct FOUNDATIONSHARED_EXPORT KiloSamplesPerSecond : public SampleRateUnit {
    KiloSamplesPerSecond(double value = 1) : SampleRateUnit(value, Kilo) {}
};

struct FOUNDATIONSHARED_EXPORT MegaSamplesPerSecond : public SampleRateUnit {
    MegaSamplesPerSecond(double value = 1) : SampleRateUnit(value, Mega) {}
};

}//namespace Planar

Q_DECLARE_METATYPE(Planar::Unit)

