#pragma once

#include "FoundationGlobal.h"

#include <QWaitCondition>
#include <QMutex>

namespace Planar {

class FOUNDATIONSHARED_EXPORT WaitCondition
{
public:
    WaitCondition(QMutex* mutex = nullptr, int maxWaitCount = 1);
    ~WaitCondition();

    bool  wait(int timeout =
                   INT_MAX);        // Ожидает перехода события в несигнальное состояние
    void  releasePendingPart();                    // Установить событие в сигнальное состояние
    void  addPendingPart();                  // Сброс события в несигнальное состояние

    void  setEnabled(bool enable);
    void  releaseAll();                         // разблокировать всё

protected:
    QMutex* _mutex;
    int _waitCount;                         // кол-во объектов, ожидающих событие
    QWaitCondition _waitCondition;
    bool _mutexCreated;
    int _maxWaitCount;
};

} // namespase Planar
