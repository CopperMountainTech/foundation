#pragma once
#include "FoundationGlobal.h"
#include <QException>


namespace Planar {

/*
 * Base - Базовый класс исключительных ситуаций
 *
 * File - Исключения при работе с файлами
 *
 * Forbidden - Запрещённая операция
 * OutOfRange - Исключение выхода индекса за границу контейнера
 * Operation - Ошибки, возвращённые из 3rdParty библиотек
 * InvalidArgument - Непригодное значение аргумента
 * MemoryAllocation - Проблемы с выделением памяти
 * NotRealized - Не реализованная операция
 * TypeMismatch - Несоответствие типов
*/

#define CUSTOM_EX(ClassName, BaseClassName) \
    class FOUNDATIONSHARED_EXPORT ClassName: public BaseClassName \
    { \
    public: \
        ClassName(const QString& msg = "<unknown>", const QString& place = "<unknown>"); \
        ClassName(const ClassName& ex); \
        void raise() const; \
        ClassName* clone() const; \
    protected: \
        using BaseClassName::_msg; \
        using BaseClassName::_place; \
        typedef BaseClassName BaseType; \
    }

#define CUSTOM_EX_IMPLEMENTATION(ClassName) \
    ClassName::ClassName(const QString& msg, const QString& place): BaseType(msg, place) \
    { \
    } \
    ClassName::ClassName(const ClassName& ex): BaseType(ex._place, ex._msg) \
    { \
    } \
    void ClassName::raise() const \
    { \
        throw* this; \
    } \
    ClassName* ClassName::clone() const \
    { \
        return new ClassName(*this); \
    }


///
/// \brief Базовый класс исключительных ситуаций
///
class FOUNDATIONSHARED_EXPORT BaseEx: public QException
{
public:
    BaseEx(const QString& msg = "<unknown>", const QString& place = "<unknown>");
    BaseEx(const BaseEx& ex);
    void raise() const;
    BaseEx* clone() const;
    QString getMsg() const
    {
        return _msg;
    }
    QString getPlace() const
    {
        return _place;
    }
protected:
    QString _msg;
    QString _place;
};

///
/// \brief Исключения при работе с файлами
///
CUSTOM_EX(FileEx, BaseEx);

///
/// \brief Запрещённая операция
///
CUSTOM_EX(ForbiddenEx, BaseEx);

///
/// \brief Исключение выхода индекса за границу контейнера
///
CUSTOM_EX(OutOfRangeEx, BaseEx);

///
/// \brief Ошибки, возвращённые из 3rdParty библиотек
///
CUSTOM_EX(OperationEx, BaseEx);

///
/// \brief Непригодное значение аргумента
///
CUSTOM_EX(InvalidArgumentEx, BaseEx);

///
/// \brief Проблемы с выделением памяти
///
CUSTOM_EX(MemoryAllocationEx, BaseEx);

///
/// \brief Не реализованная операция
///
CUSTOM_EX(NotRealizedEx, BaseEx);

///
/// \brief Несоответствие типов
///
CUSTOM_EX(TypeMismatchEx, BaseEx);

}
