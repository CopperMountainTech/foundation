#ifndef BIMAP_H
#define BIMAP_H

#include<QMap>

namespace Planar {


template<typename T1, typename T2>
class BiMap {
public:
    BiMap();
    BiMap(const std::initializer_list<std::pair<T1,T2>>& list);
    void addPair(const T1& key1, const T2& key2);
    const QMap<T1,T2>& forward() const;
    const QMap<T2,T1>& backward() const;
    T2 value2(const T1& key1) const;
    T1 value1(const T2& key2) const;
    int size() const;
private:
    QMap<T1,T2> _directMap;
    QMap<T2,T1> _reverseMap;
};


template<typename T1, typename T2>
BiMap<T1,T2>::BiMap() {}


template<typename T1, typename T2>
BiMap<T1,T2>::BiMap(const std::initializer_list<std::pair<T1,T2>>& list)
{
    for(const std::pair<T1,T2>& record : list) {
        addPair(record.first, record.second);
    }
}


template<typename T1, typename T2>
void BiMap<T1,T2>::addPair(const T1& key1, const T2& key2)
{
    //если пара добавляется повторно - ошибки не будет, добавления тоже
    if ( _directMap.contains(key1) && _reverseMap.contains(key2) && (_directMap[key1]==key2) )
        return; //такая пара уже есть

    Q_ASSERT( !_directMap.contains(key1) );
    Q_ASSERT( !_reverseMap.contains(key2) );

    _directMap[key1]=key2;
    _reverseMap[key2]=key1;
}

template<typename T1, typename T2>
const QMap<T1,T2>& BiMap<T1,T2>::forward() const
{
    return _directMap;
}

template<typename T1, typename T2>
const QMap<T2,T1>& BiMap<T1,T2>::backward() const
{
    return _reverseMap;
}


template<typename T1, typename T2>
T2 BiMap<T1,T2>::value2(const T1& key1) const
{
    return _directMap[key1];
}


template<typename T1, typename T2>
T1 BiMap<T1,T2>::value1(const T2& key2) const
{
    return _reverseMap[key2];
}

template<typename T1, typename T2>
int BiMap<T1,T2>::size() const
{
    return _directMap.size();
}


} //namespace Planar

#endif // BIMAP_H
